﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNA_Final
{
    public enum menu{new_game,Instructions,Credits,Exit,end,menu}
    public enum gamestate{Lobby,Menu,GamePlay,End}
    
    class GUI
    {
        Texture2D Lobby_tex, Menu_tex, Game_over_tex, New_but_tex, Inst_but_tex, Credits_But_tex,  Exit_but_tex,Retry_but_tex,Instruction_tex,Credits_tex,Story_tex,Back_but_tex,Enter_tex,win_tex;
        Vector2 New_but_pos, Inst_but_pos, Credits_but_pos,  Exit_but_pos,Retry_but_pos,Back_but_pos,Enter_pos,win_pos;
        Rectangle new_but_rect, Inst_but_rect, Credits_but_rect,Exit_but_rect, Retry_but_rect,screen_rect,Back_but_rect,Enter_rect,win_rect;

        board b;


        public GUI(Texture2D Newgame,Texture2D Instruction,Texture2D Credits,  Texture2D Exit, Texture2D Replay,Texture2D back_tex,Texture2D En_tex,Texture2D w_tex)
        {
            New_but_tex = Newgame;
            Inst_but_tex = Instruction;
            Credits_But_tex = Credits;
            
            Exit_but_tex = Exit;
            Retry_but_tex = Replay;
            Back_but_tex = back_tex;
            Enter_tex = En_tex;
            win_tex = w_tex;
            Inst_but_pos = new Vector2(200, 200);
            New_but_pos = new Vector2(200, 100);
            Credits_but_pos = new Vector2(200, 300);
            win_pos = new Vector2(100, 100);
            Exit_but_pos = new Vector2(200, 400);
            Back_but_pos = new Vector2(500, 410);
            Retry_but_pos = new Vector2(400,200);
            Enter_pos = new Vector2(400, 200);
        }
            
    
        
        public void init_screen(Texture2D Lobby, Texture2D Menu,Texture2D Instruction, Texture2D Credits, Texture2D GO)
        {
            Lobby_tex = Lobby;
            Menu_tex = Menu;
            Instruction_tex = Instruction;
            Credits_tex = Credits;
            Game_over_tex = GO;
            screen_rect = var.screen_rect;
        }

        public void update()
        {
            new_but_rect = new Rectangle((int)New_but_pos.X, (int)New_but_pos.Y, New_but_tex.Width, New_but_tex.Height);
            Inst_but_rect = new Rectangle((int)Inst_but_pos.X, (int)Inst_but_pos.Y, Inst_but_tex.Width, Inst_but_tex.Height);
            Credits_but_rect = new Rectangle((int)Credits_but_pos.X, (int)Credits_but_pos.Y, Credits_But_tex.Width, Credits_But_tex.Height);
            Exit_but_rect = new Rectangle((int)Exit_but_pos.X, (int)Exit_but_pos.Y, Exit_but_tex.Width, Exit_but_tex.Height);
            Retry_but_rect = new Rectangle((int)Retry_but_pos.X, (int)Retry_but_pos.Y, Retry_but_tex.Width, Retry_but_tex.Height);
            Back_but_rect = new Rectangle((int)Back_but_pos.X, (int)Back_but_pos.Y, Back_but_tex.Width, Back_but_tex.Height);
            Enter_rect = new Rectangle((int)Enter_pos.X, (int)Enter_pos.Y, Enter_tex.Width, Enter_tex.Height);
            win_rect = new Rectangle((int)win_pos.X, (int)win_pos.Y, win_tex.Width, win_tex.Height);
            if (var.gs == gamestate.Lobby)
            {
                if (Game1.mouse_rect.Intersects(Enter_rect))
                {
                    if (Game1.ms.LeftButton == ButtonState.Pressed)
                    {
                        var.gs = gamestate.Menu;
                        var.menu_obj = menu.menu;
                    }
                }
            }
            if (var.gs == gamestate.Menu)
            {
                if (var.menu_obj == menu.menu)
                {
                    if (Game1.mouse_rect.Intersects(new_but_rect))
                    {
                        if (Game1.ms.LeftButton == ButtonState.Pressed)
                        {
                            var.gs = gamestate.GamePlay;
                        }
                    }
                    else if (Game1.mouse_rect.Intersects(Inst_but_rect))
                    {
                        if (Game1.ms.LeftButton == ButtonState.Pressed)
                        {
                            var.menu_obj = menu.Instructions;
                        }
                    }
                   
                    else if (Game1.mouse_rect.Intersects(Credits_but_rect))
                    {
                        if (Game1.ms.LeftButton == ButtonState.Pressed)
                        {
                            var.menu_obj = menu.Credits;
                        }
                    }
                    else if (Game1.mouse_rect.Intersects(Exit_but_rect))
                    {
                        if (Game1.ms.LeftButton == ButtonState.Pressed)
                        {
                            var.menu_obj = menu.Exit;
                        }
                    }
                }

                if (var.menu_obj == menu.Instructions)
                {
                    if (Game1.mouse_rect.Intersects(Back_but_rect))
                    {
                        if (Game1.ms.LeftButton == ButtonState.Pressed)
                        {
                            var.menu_obj = menu.menu;
                        }
                    }
                }

               
                if (var.menu_obj == menu.Credits)
                {
                    if (Game1.mouse_rect.Intersects(Back_but_rect))
                    {
                        if (Game1.ms.LeftButton == ButtonState.Pressed)
                        {
                            var.menu_obj = menu.menu;
                        }
                    }
                }
            }
          
            if (var.gs == gamestate.End)
            {
               
                if (Game1.mouse_rect.Intersects(Back_but_rect))
                {
                    if (Game1.ms.LeftButton == ButtonState.Pressed)
                    {
                        var.menu_obj = menu.menu;
                        var.gs = gamestate.Menu;
                        b.reinit();
                    }
                }
            }

           
        }


        public void Draw(SpriteBatch sp)
        {
            if (var.gs == gamestate.Lobby)
            {
                sp.Draw(Lobby_tex, var.screen_rect, Color.White);
                sp.Draw(Enter_tex, Enter_rect, Color.White);
            }

            if (var.gs == gamestate.Menu)
            {
                sp.Draw(Menu_tex, var.screen_rect, Color.White);
                if (var.menu_obj == menu.menu)
                {
                    sp.Draw(New_but_tex, new_but_rect, Color.White);
                    sp.Draw(Exit_but_tex, Exit_but_rect, Color.White);
                    sp.Draw(Inst_but_tex, Inst_but_rect, Color.White);
                  ;
                    sp.Draw(Credits_But_tex, Credits_but_rect, Color.White);
                }
             
               
                if (var.menu_obj == menu.Instructions)
                {
                    sp.Draw(Instruction_tex, var.screen_rect, Color.White);
                    sp.Draw(Back_but_tex, Back_but_rect, Color.White);
                }
                if (var.menu_obj == menu.Credits)
                {
                    sp.Draw(Credits_tex, var.screen_rect, Color.White);
                    sp.Draw(Back_but_tex, Back_but_rect, Color.White);
                }
            }

            if (var.gs == gamestate.End)
            {
                sp.Draw(Game_over_tex, var.screen_rect, Color.White);
                sp.Draw(Back_but_tex, Back_but_rect, Color.White);
                if (var.gs == gamestate.End)
                {
                    sp.DrawString(gameplay.sf,gameplay.win, new Vector2(100, 300), Color.Green);
                }
            }
            
        }
        
    }
}
