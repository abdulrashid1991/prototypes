﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNA_Final
{
    class board
    {
        Texture2D board_tex,vampire_tex,human_tex,wolf_tex;
        public static int box_width, box_height;
        Vector2 b_pos;
        public static List<Rectangle> human_adj_rect = new List<Rectangle>();
        Rectangle bg_rect, offset_rectangle;
        

        public board(Texture2D tex, Vector2 pos, Texture2D tex_vampire, Texture2D tex_wolf,Texture2D tex_human)
        {
            board_tex = tex;
            b_pos = pos;
            vampire_tex = tex_vampire;
            wolf_tex = tex_wolf;
            human_tex = tex_human;
            var.tot_coin_count = 0;
            inithumans();

            //init cordinates
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    var.board_details[i, j].x = i;
                    var.board_details[i, j].y = j;
                }
            }

            //init coins
            for (int i = 0; i < 15; i++)
            {
                var.coin_details[i].coin_id = i;
                var.coin_details[i].coin = coin.none;
            }
            //init home
            var.Vampire_home.rchd_coins = 0;
            var.Wolf_home.rchd_coins = 0;
            
        }

        public void reinit()
        {
            var.tot_coin_count = 0;
            inithumans();

            //init cordinates
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    var.board_details[i, j].x = i;
                    var.board_details[i, j].y = j;
                }
            }

            //init coins
            for (int i = 0; i < 15; i++)
            {
                var.coin_details[i].coin_id = i;
                var.coin_details[i].coin = coin.none;
            }
            //init home
            var.Vampire_home.rchd_coins = 0;
            var.Wolf_home.rchd_coins = 0;
            var.curr_coin_count_w = 0;
            var.curr_coin_count_v = 0;
            var.max_coin_w = 0;
            var.max_coin_v = 0;
            var.Tot_coin_dead_w = 0;
            var.Tot_coin_dead_v = 0;
            var.tot_coin_rchd_v = 0;
            var.tot_coin_rchd_w = 0;
            var.tot_count_in_v = 0;
            var.tot_count_in_w = 0;


        }

        public void inithumans()
        {
            //init human coins
            var.coin_details[0].coin_id = var.tot_coin_count;
            var.coin_details[0].coin = coin.human;
            var.human_details[0].h_pos = var.coin_details[0].coin_rect = var.board_details[1, 0].box_rect;
            var.board_details[2, 0].coin = coin.human;
            var.board_details[2, 0].coin_id = var.tot_coin_count;
            var.human_details[0].isconverted = false;
            var.human_details[0].humanstate = coin.human;
            var.tot_coin_count++;

            var.coin_details[1].coin_id = var.tot_coin_count;
            var.coin_details[1].coin = coin.human;
            var.human_details[1].h_pos = var.coin_details[1].coin_rect = var.board_details[3, 3].box_rect;
            var.board_details[3, 3].coin = coin.human;
            var.board_details[3, 3].coin_id = var.tot_coin_count;
            var.human_details[1].isconverted = false;
            var.human_details[1].humanstate = coin.human;
            var.tot_coin_count++;

            var.coin_details[2].coin_id = var.tot_coin_count;
            var.coin_details[2].coin = coin.human;
            var.human_details[2].h_pos = var.coin_details[2].coin_rect = var.board_details[6, 4].box_rect;
            var.board_details[6, 4].coin = coin.human;
            var.board_details[6, 4].coin_id = var.tot_coin_count;
            var.human_details[2].isconverted = false;
            var.human_details[2].humanstate = coin.human;
            var.tot_coin_count++;
        }

        public void update()
        {
            bg_rect = new Rectangle(0, 0, Game1.sw , Game1.sh);
            offset_rectangle = new Rectangle(0,0, 50, 50);
            var.Vampire_home.home_rect = new Rectangle(0, 0, 50, 380);
            var.Wolf_home.home_rect = new Rectangle(850, 190, 50, 290);
            
            box_width = Game1.bg_width / 8;
            box_height = Game1.sh /5;
            
            for (int i = 0; i < 8; i++)//row
            {
                for (int j = 0; j < 5; j++)//column
                {
                    var.board_details[i, j].box_pt.X = box_width * i + 50;
                    var.board_details[i, j].box_pt.Y = box_height * j;
                    //createrectangles for box
                    var.board_details[i, j].box_rect = new Rectangle((int)var.board_details[i, j].box_pt.X, (int)var.board_details[i, j].box_pt.Y, box_width, box_height);
                    var.board_details[i, j].pl_id = player.none;
                }
            }
        }

        public void draw(SpriteBatch sp)
        {
            sp.Draw(board_tex, bg_rect, Color.White);
            for (int i = 0; i < 8; i++)//row
            {
                for (int j = 0; j < 5; j++)//column
                {
                    if (var.board_details[i, j].coin == coin.vampire)
                    {
                        sp.Draw(vampire_tex, var.board_details[i, j].box_rect, Color.White);
                    }

                    if (var.board_details[i, j].coin == coin.wolf)
                    {
                        sp.Draw(wolf_tex, var.board_details[i, j].box_rect , Color.White);
                    }
                    if (var.board_details[i, j].coin == coin.human)
                    {
                        sp.Draw(human_tex, var.board_details[i, j].box_rect, Color.White);
                    }
                }
            }
        }
    }
}
    
                

                

            
        

 
