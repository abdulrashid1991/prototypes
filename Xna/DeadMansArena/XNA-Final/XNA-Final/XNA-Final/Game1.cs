using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNA_Final
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        gameplay game_play;

        //mouse
        public static MouseState ms;
        public static Texture2D mouse_tex;
        public static Vector2 mspos;
        public static Rectangle mouse_rect;
       

        GUI menu_obj;

        public static int sw, sh,bg_width,bg_height;
        board board_obj;
        
        bool Game_Started;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = 900;
            //graphics.PreferredBackBufferHeight = 600;
            graphics.ApplyChanges();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            menu_obj = new GUI(Content.Load<Texture2D>("start"), Content.Load<Texture2D>("inst"), Content.Load<Texture2D>("credits"), Content.Load<Texture2D>("exit"), Content.Load<Texture2D>("retry"), Content.Load<Texture2D>("back"), Content.Load<Texture2D>("play"), Content.Load<Texture2D>("winner"));
            menu_obj.init_screen(Content.Load<Texture2D>("lobby"), Content.Load<Texture2D>("lobby"),Content.Load<Texture2D>("instpg"),Content.Load<Texture2D>("creditspg"),Content.Load<Texture2D>("lobby"));
            board_obj = new board(Content.Load<Texture2D>("bg"), Vector2.Zero, Content.Load<Texture2D>("vampire"), Content.Load<Texture2D>("wolf"), Content.Load<Texture2D>("human"));
            mouse_tex = Content.Load<Texture2D>("mouse");
            game_play = new gameplay(Content.Load<SpriteFont>("Font"));
            var.box_tex = Content.Load<Texture2D>("box");
            Game_Started = false;
            var.gs = gamestate.Lobby;
            var.menu_obj = menu.menu;
            
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            ms = Mouse.GetState();
            mspos.X = ms.X;
            mspos.Y = ms.Y;

            sw = (graphics.GraphicsDevice.PresentationParameters.BackBufferWidth);
            sh = (graphics.GraphicsDevice.PresentationParameters.BackBufferHeight);
            bg_width = sw - 100;

            mouse_rect = new Rectangle((int)ms.X, (int)ms.Y, mouse_tex.Width, mouse_tex.Height);
            var.screen_rect = new Rectangle(0, 0, sw, sh);
            menu_obj.update();

            if (var.gs == gamestate.GamePlay)
            {
                board_obj.update();
                if (mouse_rect.Intersects(var.screen_rect))
                {
                    if (ms.LeftButton == ButtonState.Pressed)
                    {
                        Game_Started = true;
                    }
                }
                if (Game_Started == true)
                {
                    game_play.game_play();
                }
            }
            if (var.menu_obj == menu.Exit)
            {
                this.Exit();
            }
          
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            menu_obj.Draw(spriteBatch);
            if (var.gs == gamestate.GamePlay)
            {
                board_obj.draw(spriteBatch);
                game_play.draw(spriteBatch);
            }
            spriteBatch.Draw(mouse_tex, mspos, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
