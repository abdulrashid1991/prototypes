﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace XNA_Final
{
    class Movment
    {
        int Temp_coin_id;
        Rectangle Temp_rect,Home_rect;
        coin Home_coin;
            
            


        List<Rectangle> adj_rectangle = new List<Rectangle>();
        
        public static bool clk;
        
        public Movment()
        {
          
        }

        public Rectangle get_clicked_rect(player p)
        {
            Rectangle rect = new Rectangle();
            
            //var.msg = Convert.ToString(p)+" Please select a box to place coin";
            if (!find_home_rect())
            {
                for (int i = 0; i < 8; i++)//row
                {
                    for (int j = 0; j < 5; j++)//column
                    {
                        if (Game1.mouse_rect.Intersects(var.board_details[i, j].box_rect))
                        {
                            if (Game1.ms.LeftButton == ButtonState.Pressed)
                            {
                                rect = var.board_details[i, j].box_rect;
                                if (gameplay.click2 == false)
                                {
                                    var.curr_x = i;
                                    var.curr_y = j;
                                    clk = true;
                                }
                                else if (gameplay.click2 == true)
                                {
                                    var.next_x = i;
                                    var.next_y = j;
                                    clk = true;
                                }
                            }
                        }
                    }
                }
            }
            else
                clk = false;
            return rect;       
        }

        public bool coin_count(coin v)
        {
            bool place_ok;

            if (var.tot_coin_count < 15)
            {
                if (var.curr_coin_count_v < 3 && v == coin.vampire && var.tot_count_in_v < var.max_coin_v)
                {
                    place_ok = true;
                }
                else if (var.curr_coin_count_w < 3 && v == coin.wolf && var.tot_count_in_w < var.max_coin_w)
                {
                    place_ok = true;
                }
                else
                    place_ok = false;
            }
            else
                place_ok = false;
            
            return place_ok;
        }

        public void update_coin(coin c)
        {
            if (var.placed == true)
            {
                var.tot_coin_count++;
                Console.WriteLine("Total Coin Count = {0}",var.tot_coin_count);
                if (c == coin.vampire)
                {
                    var.curr_coin_count_v++;
                    var.tot_count_in_v++;
                    //Console.WriteLine("Total vampire Count = {0} Curr count = {1}", var.tot_count_in_v,var.clr_coin_v);
                }
                else if (c == coin.wolf)
                {
                    var.curr_coin_count_w++;
                    var.tot_count_in_w++;
                    //Console.WriteLine("Total wolf Count = {0} Curr count = {1}", var.tot_count_in_w,var.clr_coin_w);
                }
            }
        }

        public bool place_coin(coin c, player p,Rectangle rect)
        {
            if (rect == var.board_details[7, 1].box_rect && c == coin.vampire)
            {
                //board update
                var.board_details[7, 1].coin_id = var.tot_coin_count;
                var.board_details[7, 1].pl_id = p;
                var.board_details[7, 1].coin = c;

                //coin update
                var.coin_details[var.tot_coin_count].coin = c;
                var.coin_details[var.tot_coin_count].pl_id = p;
                var.coin_details[var.tot_coin_count].coin_rect = var.board_details[7, 1].box_rect;
                var.coin_details[var.tot_coin_count].InFormation = false;
                //var.msg ="Coin Placed";
                return true;
            }
            else if (rect == var.board_details[0, 4].box_rect && c == coin.wolf)
            {
                //board update
                var.board_details[0, 4].coin_id = var.tot_coin_count;
                var.board_details[0, 4].pl_id = p;
                var.board_details[0, 4].coin = c;

                //coin update
                var.coin_details[var.tot_coin_count].coin = c;
                var.coin_details[var.tot_coin_count].pl_id = p;
                var.coin_details[var.tot_coin_count].coin_rect = var.board_details[0, 4].box_rect;
                var.coin_details[var.tot_coin_count].InFormation = false;
                //var.msg = "Coin Placed";
                return true;
            }
            else
            {
                //var.msg = "Cannot place at the selected box";
                return false;
            }
        }

        public bool select_coin()
        {
            
                Temp_coin_id = var.board_details[var.curr_x, var.curr_y].coin_id;
                Temp_rect = var.board_details[var.curr_x, var.curr_y].box_rect;
                return true;
        }

        public bool find_home_rect()
        {
            if (Game1.mouse_rect.Intersects(var.Vampire_home.home_rect))
            {
                if (Game1.ms.LeftButton == ButtonState.Pressed)
                {
                    Home_coin = var.Vampire_home.home_coin;
                    Home_rect = var.Vampire_home.home_rect;
                    return true;
                }
            }
            else if (Game1.mouse_rect.Intersects(var.Wolf_home.home_rect))
            {
                if (Game1.ms.LeftButton == ButtonState.Pressed)
                {
                    Home_coin = var.Wolf_home.home_coin;
                    Home_rect = var.Wolf_home.home_rect;
                    return true;
                }
            }
          return false;
        }

        public bool move_coin(coin c, player p,Rectangle rect)
        {
            
            if (!find_home_rect())
            {
                find_adjbox();
                int temp_frm_curr = var.board_details[var.curr_x, var.curr_y].coin_id;
                int temp_frm_next = var.board_details[var.next_x, var.next_y].coin_id;
                for (int i = 0; i < adj_rectangle.Count; i++)
                {
                    if (adj_rectangle[i] == rect)
                    {
                        if (var.board_details[var.next_x, var.next_y].coin == coin.none)
                        {
                            ////move coin
                            //board update
                            var.board_details[var.next_x, var.next_y].coin_id = Temp_coin_id;
                            var.board_details[var.next_x, var.next_y].pl_id = p;
                            var.board_details[var.next_x, var.next_y].coin = c;

                            //coin update
                            var.coin_details[Temp_coin_id].coin = c;
                            var.coin_details[Temp_coin_id].pl_id = p;
                            var.coin_details[Temp_coin_id].coin_rect = rect;
                            var.coin_details[Temp_coin_id].InFormation = false;

                            //clr old box

                            var.board_details[var.curr_x, var.curr_y].coin_id = -1;
                            var.board_details[var.curr_x, var.curr_y].pl_id = player.none;
                            var.board_details[var.curr_x, var.curr_y].coin = coin.none;
                            var.coin_details[temp_frm_curr].InFormation = false;
                        }
                        else if (var.board_details[var.next_x, var.next_y].coin == coin.human)
                        {
                            convert_human(c, p, rect);
                        }
                        else if (var.board_details[var.next_x, var.next_y].coin != c)
                        {
                            ////move coin
                            //board update
                            var.board_details[var.next_x, var.next_y].coin_id = Temp_coin_id;
                            var.board_details[var.next_x, var.next_y].pl_id = p;
                            var.board_details[var.next_x, var.next_y].coin = c;

                            //coin update
                            var.coin_details[Temp_coin_id].coin = c;
                            var.coin_details[Temp_coin_id].pl_id = p;
                            var.coin_details[Temp_coin_id].coin_rect = rect;
                            var.coin_details[Temp_coin_id].InFormation = false;

                            //clr old box
                            //temp_frm = var.board_details[var.curr_x, var.curr_y].coin_id;
                            var.board_details[var.curr_x, var.curr_y].coin_id = -1;
                            var.board_details[var.curr_x, var.curr_y].pl_id = player.none;
                            var.board_details[var.curr_x, var.curr_y].coin = coin.none;
                            var.coin_details[temp_frm_curr].InFormation = false;
                            var.coin_details[temp_frm_next].InFormation = false;
                            var.coin_details[temp_frm_next].IsDead = true;
                            var.coin_details[temp_frm_next].coin_rect = new Rectangle();
                            var.coin_details[temp_frm_next].pl_id = player.none;

                            if (var.coin_details[temp_frm_next].coin == coin.vampire)
                            {
                                var.curr_coin_count_v--;
                                var.Tot_coin_dead_v++;
                                
                            }
                            else if (var.coin_details[temp_frm_next].coin == coin.wolf)
                            {
                                var.curr_coin_count_w--;
                                var.Tot_coin_dead_w++;                                
                            }
                            else
                            {
                                return false;
                            }
                            var.coin_details[temp_frm_next].coin = coin.none;
                        }
                        deselect();
                        return true;
                    }

                }
            }
            else if(find_home_rect())
            {
                if (Home_coin == var.board_details[var.curr_x, var.curr_y].coin)
                {
                    int temp_id = var.board_details[var.curr_x, var.curr_y].coin_id;
                    if (Home_coin == coin.vampire)
                    {
                        if (var.board_details[var.curr_x, var.curr_y].box_rect == var.board_details[0, 2].box_rect )
                        {
                            if(coin_reached(temp_id, Home_coin))
                            {
                                var.placed = true;

                                return true;
                            }

                        }
                    }
                    if (Home_coin == coin.wolf)
                    {
                        if (var.board_details[var.curr_x, var.curr_y].box_rect == var.board_details[7, 3].box_rect)
                        {
                            if (coin_reached(temp_id, Home_coin))
                            {
                                var.placed = true;
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool coin_reached(int coin_id,coin c)
        {
            var.coin_details[coin_id].IsReached = true;
            if (c == coin.vampire)
            {
                var.curr_coin_count_v--;
                var.tot_coin_rchd_v++;
                
            }
            else if (c == coin.wolf)
            {
                var.curr_coin_count_w--;
                var.tot_coin_rchd_w++;
            }
            var.coin_details[coin_id].InFormation = false;
            var.coin_details[coin_id].coin_rect = Home_rect;
            var.board_details[var.curr_x, var.curr_y].coin_id = -1;
            var.board_details[var.curr_x, var.curr_y].coin = coin.none;
            var.board_details[var.curr_x, var.curr_y].pl_id = player.none;
            deselect();
            return true;
        }

        public void convert_human(coin c,player p,Rectangle rect)
        {
            int temp_coin_id = var.board_details[var.next_x, var.next_y].coin_id;

            var.coin_details[temp_coin_id].coin = c;
            //var.human_details[0].h_pos = var.coin_details[0].coin_rect = var.board_details[1, 0].box_rect;
            var.human_details[temp_coin_id].isconverted = true;
            var.human_details[temp_coin_id].humanstate = c;

            //board update
            var.board_details[var.next_x, var.next_y].coin_id = temp_coin_id;
            var.board_details[var.next_x, var.next_y].pl_id = p;
            var.board_details[var.next_x, var.next_y].coin = c;

            //coin update
            var.coin_details[temp_coin_id].coin = c;
            var.coin_details[temp_coin_id].pl_id = p;
            var.coin_details[temp_coin_id].coin_rect = rect;
            if (c == coin.vampire)
            {
                var.max_coin_v++;
                var.tot_count_in_v++;
            }
            else if(c == coin.wolf)
            {
                var.max_coin_w++;
                var.tot_count_in_w++;
            }
            
        }

        public void check_formation()
        {
            //List<Rectangle> formation_rect = new List<Rectangle>();
            int temp_id1, temp_id2;
            for (int i = 0; i < 8; i++)//row
            {
                for (int j = 0; j < 5; j++)//column
                {
                    if (var.board_details[i, j].coin == coin.vampire)
                    {
                        temp_id1 = var.board_details[i, j].coin_id;
                        if (j != 4 && var.board_details[i, j + 1].coin == coin.vampire)
                        {
                            temp_id2 = var.board_details[i, j + 1].coin_id;
                            var.coin_details[temp_id1].InFormation = true;
                            var.coin_details[temp_id2].InFormation = true;
                            temp_id2 = -1;
                        }
                        if (j != 0 && var.board_details[i, j - 1].coin == coin.vampire)
                        {
                            temp_id2 = var.board_details[i, j - 1].coin_id;
                            var.coin_details[temp_id1].InFormation = true;
                            var.coin_details[temp_id2].InFormation = true;
                            temp_id2 = -1;
                        }
                        if (i != 7 && var.board_details[i + 1, j].coin == coin.vampire)
                        {
                            temp_id2 = var.board_details[i + 1, j].coin_id;
                            var.coin_details[temp_id1].InFormation = true;
                            var.coin_details[temp_id2].InFormation = true;
                            temp_id2 = -1;
                        }
                        if (i != 0 && var.board_details[i - 1, j].coin == coin.vampire)
                        {
                            temp_id2 = var.board_details[i - 1, j].coin_id;
                            var.coin_details[temp_id1].InFormation = true;
                            var.coin_details[temp_id2].InFormation = true;
                            temp_id2 = -1;
                        }
                        temp_id1 = -1;
                    }
                    else if (var.board_details[i, j].coin == coin.wolf)
                    {
                        temp_id1 = var.board_details[i, j].coin_id;
                        if (i != 7 && j != 4 && var.board_details[i + 1, j + 1].coin == coin.wolf)
                        {
                            temp_id2 = var.board_details[i + 1, j + 1].coin_id;
                            var.coin_details[temp_id1].InFormation = true;
                            var.coin_details[temp_id2].InFormation = true;
                            temp_id2 = -1;
                        }
                        if (i != 0 && j != 4 && var.board_details[i - 1, j + 1].coin == coin.wolf)
                        {
                            temp_id2 = var.board_details[i - 1, j + 1].coin_id;
                            var.coin_details[temp_id1].InFormation = true;
                            var.coin_details[temp_id2].InFormation = true;
                            temp_id2 = -1;
                        }
                        if (i != 7 && j != 0 && var.board_details[i + 1, j - 1].coin == coin.wolf)
                        {
                            temp_id2 = var.board_details[i + 1, j - 1].coin_id;
                            var.coin_details[temp_id1].InFormation = true;
                            var.coin_details[temp_id2].InFormation = true;
                            temp_id2 = -1;
                        }
                        if (i != 0 && j != 0 && var.board_details[i - 1, j - 1].coin == coin.wolf)
                        {
                            temp_id2 = var.board_details[i - 1, j - 1].coin_id;
                            var.coin_details[temp_id1].InFormation = true;
                            var.coin_details[temp_id2].InFormation = true;
                            temp_id2 = -1;
                        }
                        temp_id1 = -1;
                    }
                }
            }
        }

        public bool check_kill(int c_x,int c_y,int n_x,int n_y)
        {
            int temp_coin_id_n = var.board_details[n_x, n_y].coin_id;
            int temp_coin_id_c = var.board_details[c_x, c_y].coin_id;

            if (var.board_details[n_x, n_y].box_rect == var.board_details[7, 1].box_rect && var.board_details[c_x, c_y].coin == coin.wolf)
            {
                return false;
            }

            if (var.board_details[n_x, n_y].box_rect == var.board_details[0, 4].box_rect && var.board_details[c_x, c_y].coin == coin.vampire)
            {
                return false;
            }

            if (var.board_details[n_x, n_y].coin == coin.human || var.board_details[n_x, n_y].coin == coin.none)
            {
                return true;
            }
            else if (var.board_details[c_x, c_y].coin != var.board_details[n_x, n_y].coin)
            {
                //if (temp_coin_id_c != -1 && var.coin_details[temp_coin_id_c].InFormation == true)
                {
                    if (temp_coin_id_n != -1 && var.coin_details[temp_coin_id_n].InFormation == false)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public List<Rectangle> find_adjbox()
        {

            int m_x, m_y;
           
            m_x = var.curr_x;
            m_y = var.curr_y;
            

            if (var.board_details[m_x, m_y].coin == coin.vampire)
            {
                if (gameplay.click2 == true)
                {
                    if (m_y != 4 && (check_kill(m_x,m_y,m_x, m_y + 1)))
                    {
                        adj_rectangle.Add(var.board_details[m_x, m_y + 1].box_rect);
                    }
                    if (m_y != 0 && (check_kill(m_x, m_y, m_x, m_y - 1)))
                    {
                        adj_rectangle.Add(var.board_details[m_x, m_y - 1].box_rect);
                    }
                    if (m_x != 0 && (check_kill(m_x, m_y, m_x - 1, m_y )))
                    {
                        adj_rectangle.Add(var.board_details[m_x - 1, m_y].box_rect);
                    }
                    if (m_x != 7 && (check_kill(m_x, m_y, m_x + 1, m_y)))
                    {
                        adj_rectangle.Add(var.board_details[m_x + 1, m_y].box_rect);
                    }
                }
            }

            else if (var.board_details[m_x, m_y].coin == coin.wolf)
            {
                if (gameplay.click2 == true)
                {
                    if (m_x < 7 && m_y < 4 && (check_kill(m_x, m_y, m_x + 1, m_y + 1)))
                    {
                        adj_rectangle.Add(var.board_details[m_x + 1, m_y + 1].box_rect);
                    }
                    if (m_x > 0 && m_y > 0 && (check_kill(m_x, m_y, m_x - 1, m_y - 1)))
                    {
                        adj_rectangle.Add(var.board_details[m_x - 1, m_y - 1].box_rect);
                    }
                    if (m_x > 0 && m_y < 4 && (check_kill(m_x, m_y, m_x - 1, m_y + 1)))
                    {
                        adj_rectangle.Add(var.board_details[m_x - 1, m_y + 1].box_rect);
                    }
                    if (m_y > 0 && m_x < 7 && (check_kill(m_x, m_y, m_x + 1, m_y - 1)))
                    {
                        adj_rectangle.Add(var.board_details[m_x + 1, m_y - 1].box_rect);
                    }
                }
            }
            return adj_rectangle;
        }

        void deselect()
        {
           gameplay.click2 = false;
           Temp_coin_id = -1;
           Temp_rect = new Rectangle();
           adj_rectangle.Clear();
           var.curr_x = var.curr_y = var.next_x = var.next_y = 0;
        }

        public void Draw(SpriteBatch sp)
        {
            sp.Draw(var.box_tex, Temp_rect, Color.White);
            if (gameplay.click2 == true)
            {
                for (int i = 0; i < adj_rectangle.Count; i++)
                {
                    sp.Draw(var.box_tex, adj_rectangle[i], Color.Salmon);
                }
            }
            
        }
    }
}
