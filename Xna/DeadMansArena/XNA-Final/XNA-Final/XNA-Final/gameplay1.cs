﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNA_Final
{
    class gameplay
    {
        //static int turn = 0;
        getdetails details;
        Movment mov;
        int t;
        public static coin winner;
        public static string win;
        public static bool click2 = false;
        public static bool click1 = false;
        public static bool GO = false;

        public static SpriteFont sf;

        public gameplay(SpriteFont s)
        {
            t = 0;
            details = new getdetails();
            mov = new Movment();
            sf = s;
        }


        public void game_play()
        {
            
            details.init_details("Player1", "Player2");

            
                if (t == 0)
                {
                    var.msg = Convert.ToString(var.player_details[t].id);
                    play(t);
                    if (var.placed == true)
                    {
                        t = 1;
                        Console.WriteLine("Vampire:totcoin={0},incoins={1},Rchdcoin={2},deadcoins={03},currcoin={4},Maxcoin={9}|| Wolf:incoins={5},Rchdcoin={6},deadcoins={7},currcoin={8},maxcoin={10}", var.tot_coin_count, var.tot_count_in_v, var.tot_coin_rchd_v, var.Tot_coin_dead_v, var.curr_coin_count_v, var.tot_count_in_w, var.tot_coin_rchd_w, var.Tot_coin_dead_w, var.curr_coin_count_w,var.max_coin_v ,var.max_coin_w);
                        var.placed = false;
                    }
                    mov.check_formation();
                    Declare_winner();
                    //Console.WriteLine("Vampire:totcoin={0},incoins={1},Rchdcoin={2},deadcoins={03},currcoin={4}|| Wolf:incoins={5},Rchdcoin={6},deadcoins={7},currcoin={8}", var.tot_coin_count, var.tot_count_in_v, var.tot_coin_rchd_v, var.Tot_coin_dead_v, var.curr_coin_count_v, var.tot_count_in_w, var.tot_coin_rchd_w, var.Tot_coin_dead_w, var.curr_coin_count_w);
                }
                else if (t == 1)
                {
                    var.msg = Convert.ToString(var.player_details[t].id);
                    play(t);
                    if (var.placed == true)
                    {
                        t = 0;
                        Console.WriteLine("Vampire:totcoin={0},incoins={1},Rchdcoin={2},deadcoins={03},currcoin={4},maxcoin={9}|| Wolf:incoins={5},Rchdcoin={6},deadcoins={7},currcoin={8},maxcoin={10}", var.tot_coin_count, var.tot_count_in_v, var.tot_coin_rchd_v, var.Tot_coin_dead_v, var.curr_coin_count_v, var.tot_count_in_w, var.tot_coin_rchd_w, var.Tot_coin_dead_w, var.curr_coin_count_w, var.max_coin_v, var.max_coin_w);
                        var.placed = false;
                    }
                    mov.check_formation();
                    Declare_winner();
                    //Console.WriteLine("Vampire:totcoin={0},incoins={1},Rchdcoin={2},deadcoins={03},currcoin={4}|| Wolf:incoins={5},Rchdcoin={6},deadcoins={7},currcoin={8}", var.tot_coin_count, var.tot_count_in_v, var.tot_coin_rchd_v, var.Tot_coin_dead_v, var.curr_coin_count_v, var.tot_count_in_w, var.tot_coin_rchd_w, var.Tot_coin_dead_w, var.curr_coin_count_w);
                }
            

        }

        public bool Game_over()
        {
            if ((var.tot_count_in_v == var.max_coin_v ) || (var.tot_count_in_w == var.max_coin_w ))
            {
                if (var.tot_coin_rchd_v + var.Tot_coin_dead_v == var.max_coin_v || var.tot_coin_rchd_w + var.Tot_coin_dead_w == var.max_coin_w )
                {
                    if (var.tot_coin_rchd_v == var.tot_coin_rchd_w)
                    {
                        winner = coin.none;
                        return true;
                    }
                    else if (var.tot_coin_rchd_v < var.Tot_coin_dead_w)
                    {
                        winner = coin.wolf;
                        return true;
                    }
                    else
                    {
                        winner = coin.vampire;
                        return true;
                    }
                    
                }
            }
            return false;
        }

        public void Declare_winner()
        {
            if (Game_over())
            {

                if (winner == var.player_details[0].coin)
                {
                    win = "The Winner is " + var.player_details[0].name;
                }
                else if (winner == var.player_details[1].coin)
                {
                    win = "The Winner is " + var.player_details[1].name;
                }
                else if (winner == coin.none)
                {
                    win = "The match is Draw";
                }
                var.gs = gamestate.End;
            }
        }


        public void play(int t)
        {
            coin curr_coin;
            player curr_player;

            Rectangle curr_rect, next_rect;


            curr_coin = var.player_details[t].coin;
            curr_player = var.player_details[t].id;

            //if (t == 0)
            {
                curr_rect = mov.get_clicked_rect(curr_player);
            }
            //else if (t == 1)
            {
                //curr_rect = //ai.single_mov(curr_player);
            }
            
                if (curr_rect != null)
                {
                    click1 = true;
                }
                if (var.board_details[var.curr_x, var.curr_y].coin == coin.none && click2 == false)
                {
                    if (mov.coin_count(curr_coin))
                    {
                        if (click1 == true)
                        {
                            if (mov.place_coin(curr_coin, curr_player, curr_rect))
                            {
                                var.placed = true;
                                mov.update_coin(curr_coin);
                                click1 = false;
                            }
                        }
                        else
                        {
                            var.placed = false;
                            click1 = false;
                        }
                    }
                }
                else if (var.board_details[var.curr_x, var.curr_y].coin == curr_coin && click2 == false)
                {

                    if (mov.select_coin())
                    {
                        click2 = true;
                    }

                }

                if (click2 == true)
                {
                    //if (t == 0)
                    {
                        next_rect = mov.get_clicked_rect(curr_player);
                    }
                    //else if (t == 1)
                    {
                        //next_rect = ai.move_coin(curr_player);
                    }
                    if (mov.move_coin(curr_coin, curr_player, next_rect))
                    {
                        click2 = false;
                        var.placed = true;
                    }
                    else
                    {
                        var.placed = false;
                    }
                }
            }
            
        
        public void draw(SpriteBatch sp)
        {
            sp.DrawString(sf, var.msg, Game1.mspos + new Vector2(10, 20), Color.Green);
            sp.DrawString(sf, Convert.ToString(var.tot_coin_rchd_v), new Vector2(20, 25), Color.Green);
            sp.DrawString(sf, Convert.ToString(var.tot_coin_rchd_w), new Vector2(870, 435), Color.Green); 
            mov.Draw(sp);
        }
    }
}
