﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNA_Final
{
    class gameplay
    {
        //static int turn = 0;
        getdetails details;
        Movment mov;
        int t;
        public static bool click2 = false;
        public static bool click1 = false;

        SpriteFont sf;

        public gameplay(SpriteFont s)
        {
            t = 0;
            details = new getdetails();
            mov = new Movment();
            sf = s;
        }


        public void game_play()
        {
            
            details.init_details("Player1", "Player2");


            if (t == 0)
            {
                play(t);
                if(var.placed == true)
                {
                    t = 1;
                    var.placed = false;
                }
            }
            else if (t == 1)
            {
                play(t);
                if (var.placed == true)
                {
                    t = 0;
                    var.placed = false;
                }
            }

        }


        public void play(int t)
        {
            coin curr_coin;
            player curr_player;
            
            

            Rectangle curr_rect,next_rect;

           
            curr_coin = var.player_details[t].coin;
            curr_player = var.player_details[t].id;
            
            
            curr_rect = mov.get_clicked_rect(curr_player);
            if (curr_rect != null)
            {
                click1 = true;
            }
            if (var.board_details[var.curr_x, var.curr_y].coin == coin.none && click2 == false)
            {
                if (mov.coin_count(curr_coin))
                {
                    if (click1 == true)
                    {
                        if (mov.place_coin(curr_coin, curr_player, curr_rect))
                        {
                            var.placed = true;
                            mov.update_coin(curr_coin);
                            click1 = false;
                        }
                    }
                    else
                    {
                        var.placed = false;
                        click1 = false;
                    }
                }
            }
            else if (var.board_details[var.curr_x, var.curr_y].coin == curr_coin && click2 == false)
            {

                //if (click1 == false)
                {
                    if (mov.select_coin())
                    {
                        click2 = true;
                    }
                }
            }

            if (click2 == true)
            {
                next_rect = mov.get_clicked_rect(curr_player);
                if (mov.move_coin(curr_coin, curr_player, next_rect))
                {
                    click2 = false;
                    var.placed = true;
                }
                else
                {
                    var.placed = false;
                }
            }
        }



        public void draw(SpriteBatch sp)
        {
            sp.DrawString(sf, var.msg, new Vector2(20, 20), Color.Black);
            mov.Draw(sp);
        }
    }
}
