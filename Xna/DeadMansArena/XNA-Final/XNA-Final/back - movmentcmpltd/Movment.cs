﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace XNA_Final
{
    class Movment
    {
        int Temp_coin_id;
        Rectangle Temp_rect; 
        //Rectangle[] adj_rectangle = new Rectangle[4];
        List<Rectangle> adj_rectangle = new List<Rectangle>();
        bool clk;
        
        public Movment()
        {
          
        }

        public Rectangle get_clicked_rect(player p)
        {
            Rectangle rect = new Rectangle();
            
            var.msg = Convert.ToString(p)+" Please select a box to place coin";
            for (int i = 0; i < 8; i++)//row
            {
                for (int j = 0; j < 5; j++)//column
                {
                    if (Game1.mouse_rect.Intersects(var.board_details[i, j].box_rect))
                    {
                        if (Game1.ms.LeftButton == ButtonState.Pressed)
                        {
                            rect = var.board_details[i, j].box_rect;
                            if (gameplay.click2 == false)
                            {
                                var.curr_x = i;
                                var.curr_y = j;
                                clk = false;
                            }
                            else if (gameplay.click2 == true)
                            {
                                var.next_x = i;
                                var.next_y = j;
                            }
                        }
                    }
                }
            }
            return rect;       
        }

        public bool coin_count(coin v)
        {
            bool place_ok;

            if (var.tot_coin_count < 15)
            {
                if (var.curr_coin_count_v < 3 && v == coin.vampire)
                {
                    place_ok = true;
                }
                else if (var.curr_coin_count_w < 3 && v == coin.wolf)
                {
                    place_ok = true;
                }
                else
                    place_ok = false;
            }
            else
                place_ok = false;
            
            return place_ok;
        }

        public void update_coin(coin c)
        {
            if (var.placed == true)
            {
                var.tot_coin_count++;
                if (c == coin.vampire)
                {
                    var.curr_coin_count_v++;
                }
                else if (c == coin.wolf)
                {
                    var.curr_coin_count_w++;
                }
            }
        }

        public bool place_coin(coin c, player p,Rectangle rect)
        {
            if (rect == var.board_details[7, 1].box_rect && c == coin.vampire)
            {
                //board update
                var.board_details[7, 1].coin_id = var.tot_coin_count;
                var.board_details[7, 1].pl_id = p;
                var.board_details[7, 1].coin = c;

                //coin update
                var.coin_details[var.tot_coin_count].coin = c;
                var.coin_details[var.tot_coin_count].pl_id = p;
                var.coin_details[var.tot_coin_count].coin_rect = var.board_details[7, 1].box_rect;
                var.msg ="Coin Placed";
                return true;
            }
            else if (rect == var.board_details[0, 4].box_rect && c == coin.wolf)
            {
                //board update
                var.board_details[0, 4].coin_id = var.tot_coin_count;
                var.board_details[0, 4].pl_id = p;
                var.board_details[0, 4].coin = c;

                //coin update
                var.coin_details[var.tot_coin_count].coin = c;
                var.coin_details[var.tot_coin_count].pl_id = p;
                var.coin_details[var.tot_coin_count].coin_rect = var.board_details[0, 4].box_rect;
                var.msg = "Coin Placed";
                return true;
            }
            else
            {
                var.msg = "Cannot place at the selected box";
                return false;
            }
        }

        public bool select_coin()
        {
            //if (gameplay.click1 == false)
            {
                Temp_coin_id = var.board_details[var.curr_x, var.curr_y].coin_id;
                Temp_rect = var.board_details[var.curr_x, var.curr_y].box_rect;
                return true;
            }
            //else
               // return false;
        }

        public bool move_coin(coin c, player p,Rectangle rect)
        {
            
                find_adjbox();
                for (int i = 0; i < adj_rectangle.Count; i++)
                {
                    if (adj_rectangle[i] == rect)
                    {
                        ////move coin
                        //board update
                        var.board_details[var.next_x, var.next_y].coin_id = Temp_coin_id;
                        var.board_details[var.next_x, var.next_y].pl_id = p;
                        var.board_details[var.next_x, var.next_y].coin = c;

                        //coin update
                        var.coin_details[Temp_coin_id].coin = c;
                        var.coin_details[Temp_coin_id].pl_id = p;
                        var.coin_details[Temp_coin_id].coin_rect = rect;

                        //clr old box
                        var.board_details[var.curr_x, var.curr_y].coin_id = -1;
                        var.board_details[var.curr_x, var.curr_y].pl_id = player.none;
                        var.board_details[var.curr_x, var.curr_y].coin = coin.none;
                        deselect();
                        return true;
                    }

                }
            
            return false;
        }


        public void find_adjbox()
        {
            
            int m_x, m_y;
            m_x = var.curr_x;
            m_y = var.curr_y;

            if (var.board_details[m_x, m_y].coin == coin.vampire)
            {
                if (gameplay.click2 == true)
                {
                    if (m_y != 4 && var.board_details[m_x, m_y + 1].coin == coin.none)
                    {
                        adj_rectangle.Add(var.board_details[m_x, m_y + 1].box_rect);
                    }
                    if (m_y != 0 && var.board_details[m_x, m_y - 1].coin == coin.none)
                    {
                        adj_rectangle.Add(var.board_details[m_x, m_y - 1].box_rect);
                    }
                    if (m_x != 0 && var.board_details[m_x - 1, m_y].coin == coin.none)
                    {
                        adj_rectangle.Add(var.board_details[m_x - 1, m_y].box_rect);
                    }
                    if (m_x != 7 && var.board_details[m_x + 1, m_y].coin == coin.none)
                    {
                        adj_rectangle.Add(var.board_details[m_x + 1, m_y].box_rect);
                    }
                }
            }

            else if (var.board_details[m_x, m_y].coin == coin.wolf)
            {
                if (gameplay.click2 == true)
                {
                    if (m_x < 7 && m_y < 4 && var.board_details[m_x + 1, m_y + 1].coin == coin.none)
                    {
                        adj_rectangle.Add(var.board_details[m_x + 1, m_y + 1].box_rect);
                    }
                    if (m_x > 0 && m_y > 0 && var.board_details[m_x - 1, m_y - 1].coin == coin.none)
                    {
                        adj_rectangle.Add(var.board_details[m_x - 1, m_y - 1].box_rect);
                    }
                    if (m_x > 0 && m_y < 4 && var.board_details[m_x - 1, m_y + 1].coin == coin.none)
                    {
                        adj_rectangle.Add(var.board_details[m_x - 1, m_y + 1].box_rect);
                    }
                    if (m_y > 0 && m_x < 7 && var.board_details[m_x + 1, m_y - 1].coin == coin.none)
                    {
                        adj_rectangle.Add(var.board_details[m_x + 1, m_y - 1].box_rect);
                    }
                }

            }

        }

        void deselect()
        {
            gameplay.click2 = false;
           Temp_coin_id = -1;
           Temp_rect = new Rectangle();
           adj_rectangle.Clear();
        }

        public void Draw(SpriteBatch sp)
        {
            sp.Draw(var.box_tex, Temp_rect, Color.Blue);
            if (gameplay.click2 == true)
            {
                for (int i = 0; i < adj_rectangle.Count; i++)
                {
                    sp.Draw(var.box_tex, adj_rectangle[i], Color.Yellow);
                }
            }
        }
    }
}
