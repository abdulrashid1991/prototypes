﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNA_Final
{
    public enum coin { none, vampire, wolf, human }
    public enum player { player1, player2,none }
    public enum gamestate { lobby, menu, help, gameplay, Game_end }
    public enum menustate { newgame, instructions, credits, exit }

    public struct human
    {
        Rectangle h_pos;
        Texture2D h_tex;
        bool isconverted;
        public coin humanstate;
    }

    public struct player_det
    {
        public player id;
        public string name;
        public coin coin;

    }

    public struct coin_details
    {
        public int coin_id;
        public coin coin;
        public player pl_id;
        public Rectangle coin_rect;
        bool IsAlive;
        bool IsReached;
    }



    public struct board_var
    {
        public Vector2 box_pt;
        public int x, y;
        public coin coin;
        public int coin_id;
        public Rectangle box_rect;
        public player pl_id;

    }


    class structs
    {

    }
}
