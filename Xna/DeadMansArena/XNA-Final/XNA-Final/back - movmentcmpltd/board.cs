﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNA_Final
{
    class board
    {
        Texture2D board_tex;
        Texture2D vampire_tex;
        Vector2 b_pos;
        Texture2D wolf_tex;
        public static int box_width, box_height;


        public board(Texture2D tex, Vector2 pos, Texture2D tex1, Texture2D tex2)
        {

            
            board_tex = tex;
            b_pos = pos;
            vampire_tex = tex1;
            wolf_tex = tex2;
            var.tot_coin_count = 0;
            inithumans();

            //init cordinates
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    var.board_details[i, j].x = i;
                    var.board_details[i, j].y = j;
                }
            }

            //init coins
            for (int i = 0; i < 15; i++)
            {
                var.coin_details[i].coin_id = i;
                var.coin_details[i].coin = coin.none;
            }

        }

        public void inithumans()
        {
            //init human coins
            var.coin_details[0].coin_id = var.tot_coin_count;
            var.coin_details[0].coin = coin.human;
            var.coin_details[0].coin_rect = var.board_details[1, 0].box_rect;
            var.board_details[1, 0].coin = coin.human;
            var.board_details[1, 0].coin_id = var.tot_coin_count;
            var.tot_coin_count++;

            var.coin_details[1].coin_id = var.tot_coin_count;
            var.coin_details[1].coin = coin.human;
            var.coin_details[1].coin_rect = var.board_details[3, 3].box_rect;
            var.board_details[3, 3].coin = coin.human;
            var.board_details[3, 3].coin_id = var.tot_coin_count;
            var.tot_coin_count++;

            var.coin_details[2].coin_id = var.tot_coin_count;
            var.coin_details[2].coin = coin.human;
            var.coin_details[2].coin_rect = var.board_details[6, 4].box_rect;
            var.board_details[6, 4].coin = coin.human;
            var.board_details[6, 4].coin_id = var.tot_coin_count;
            var.tot_coin_count++;

            //null boxes code
        }

        public void update()
        {


            box_width = Game1.sw / 8;
            box_height = Game1.sh / 5;

            for (int i = 0; i < 8; i++)//row
            {
                for (int j = 0; j < 5; j++)//column
                {
                    var.board_details[i, j].box_pt.X = box_width * i;
                    var.board_details[i, j].box_pt.Y = box_height * j;
                    //createrectangles for box
                    var.board_details[i, j].box_rect = new Rectangle((int)var.board_details[i, j].box_pt.X, (int)var.board_details[i, j].box_pt.Y, box_width, box_height);
                    var.board_details[i, j].pl_id = player.none;
                }
            }
        }

       


        public void draw(SpriteBatch sp)
        {
            sp.Draw(board_tex, var.screen_rect, Color.White);
            for (int i = 0; i < 8; i++)//row
            {
                for (int j = 0; j < 5; j++)//column
                {
                    if (var.board_details[i, j].coin == coin.vampire)
                    {
                        sp.Draw(vampire_tex,var.board_details[i,j].box_rect, Color.White);
                    }

                    if (var.board_details[i, j].coin == coin.wolf)
                    {
                        sp.Draw(wolf_tex, var.board_details[i, j].box_rect, Color.White);
                    }
                }
            }
        }
    }
}
    
                

                

            
        

 
