﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace S4GP_Phyics
{
    class camera
    {
        public Matrix transform;
        public Vector2 camera_pos;

        public camera()
        {
            camera_pos = Vector2.Zero;
        }
        public Matrix getcamerapos()
        {
            transform = Matrix.CreateTranslation(-camera_pos.X, -camera_pos.Y, 0);
            return transform;
        }
    }
}
