﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;


namespace S4GP_Phyics
{
    public class Particle
    {
        Texture2D Texture ;       
        Vector2 Position,Velocity;
        float Size;
        Color Color;
        public int LifeTime;
        

        public Particle(Texture2D texture, Vector2 position, Vector2 velocity, Color color, float size, int lifetime)
        {
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Color = color;
            Size = size;
            LifeTime = lifetime;
        }

        public void Update()
        {
            LifeTime--;
            Position += Velocity;
           
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {

           spriteBatch.Draw(Texture, Position,null , Color, 0 , Vector2 .Zero , Size, SpriteEffects.None, 0f);
           
        }

    }
}
