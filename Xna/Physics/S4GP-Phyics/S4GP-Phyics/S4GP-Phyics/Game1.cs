using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace S4GP_Phyics
{
    public enum dir { left, straight, right};

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D Flight_off, bg, Flight_left, Flight_right,Missile_tex, exp1_tex,exp2_tex,exp_animate;
        Vector2 Flight_pos, Missile_pos, Flight_velocity, Missile_velocity, Gravity, exp_pos;
        Vector2 smokePos1, smokePos2;
        Texture2D smokeTexture,the_end;
        public static Random rnd = new Random();


        SoundEffect Lfl_takeoff, Lmiss_takeoff, Lmissile_hit, Lexp1, Lexp2;
        SoundEffectInstance fl_takeoff,miss_takeoff,missile_hit,exp1,exp2;

        Rectangle Flight_rect, Missile_rect;

        KeyboardState ks;
        MouseState ms;
        Vector2 mspos;

        camera cam;
        bool is_Attacked,launch_mis;
        
        bool stop;

        float fl_width, fl_height;
        float time,black;
        int animatetime,te;
        int fx, fy, fwidth, fheight, cframe, totframe;
        Rectangle exp_rect;

        ParticleEngine fire_engine, explotion_engine;
        List<Vector2> smokeList1 = new List<Vector2>();
        List<Vector2> smokeList2 = new List<Vector2>();

        public void Animate_explosion(int totalframes, GameTime gt)
        {
            totframe = totalframes;
            animatetime = (int)gt.TotalGameTime.Milliseconds;
            if (animatetime % 100 == 0)
            {
                //finding current frame which is to be drawn
                if (cframe <= totalframes)
                {
                    cframe++;
                }
                
                fx = fwidth * cframe;//x of the current frame
                fy = 0;//y of the current frame
                fwidth = (exp_animate.Width / totalframes);//width of current frame
                fheight = exp_animate.Height;//height of current frame
                exp_rect = new Rectangle(fx, fy, fwidth, fheight);
            }
        }
       
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
      protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = 480;
            graphics.PreferredBackBufferHeight = 620;
            graphics.ApplyChanges();
            Window.Title = "2D Flight sim";
            base.Initialize();
            
        }

      protected override void LoadContent()
      {
          spriteBatch = new SpriteBatch(GraphicsDevice);
          Flight_velocity = new Vector2(0, 0);
          Gravity = new Vector2(0.4f, 0.4f);
          cam = new camera();
          Flight_off = Content.Load<Texture2D>("fl_st");
         
          bg = Content.Load<Texture2D>("bg");

          exp2_tex = Content.Load<Texture2D>("e");
          exp_animate = Content.Load<Texture2D>("exp");
          Missile_tex = Content.Load<Texture2D>("Rocket");
          smokeTexture = Content.Load<Texture2D>("smoke");

          Missile_velocity = new Vector2(0, 0);
          Missile_pos = Flight_pos = new Vector2(189, 550);
          fl_width = 40;
          fl_height = 55;
          time = 0.50f;
          is_Attacked = false;
          launch_mis = false;
          stop = false;
          black = 1f;
          explotion_engine = new ParticleEngine(exp2_tex);
          te = 0;
          //sound
          Lfl_takeoff = Content.Load<SoundEffect>("flight");
          fl_takeoff = Lfl_takeoff.CreateInstance();
          Lmiss_takeoff = Content.Load<SoundEffect>("missliefollow");
          miss_takeoff = Lmiss_takeoff.CreateInstance();
          Lmissile_hit = Content.Load<SoundEffect>("missile_exp");
          missile_hit = Lmissile_hit.CreateInstance();
          Lexp1 = Content.Load<SoundEffect>("final_exp");
          exp1 = Lexp1.CreateInstance();
         
          

      }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            ks = Keyboard.GetState();
            
        
                if (Flight_velocity.Y > 2.45 )
                {
                    cam.camera_pos.Y = Flight_pos.Y - 250;
                    if (is_Attacked == false)
                    {
                        Flight_velocity.Y += 0.02f * time;
                        //full movment
                    }
                }
                else
                {
                    if (is_Attacked == false)
                    {
                        Flight_velocity.Y += 0.01f * time;
                        //init movment
                    }
                    if (is_Attacked == true)
                    {
                        
                        //missle atcked
                    }
                }

                if (Flight_velocity.Y > 1.72 )
                {
                    if (fl_width < 150 && fl_height < 190 && is_Attacked == false)
                    {
                        fl_width += Gravity.X * time;
                        fl_height += Gravity.Y * time;
                        //take off
                    }
                    else
                    {
                        if (Missile_rect.Intersects(Flight_rect))
                        {
                            is_Attacked = true;
                            miss_takeoff.Pause();
                            missile_hit.Play();
                            fl_takeoff.Pause();
                            //attack detection
                        }
                        Missile_velocity.Y += 0.4f * time;
                        launch_mis = true;
                        if (is_Attacked == false)
                        {
                            miss_takeoff.Play();
                        }
                    }
                }
                        if (is_Attacked == true )
                        {
                            if (Flight_velocity.Y >= 0)
                            {
                                Flight_velocity.Y -= 0.01f;
                                //decrease velocity for flight
                            }
                            else
                            {
                                Flight_velocity.Y = 0;
                                
                            }

                            exp_pos = Flight_pos + new Vector2(32,50);

                            if (cframe <= totframe)
                            {
                                Animate_explosion(13, gameTime);
                            }
                            else
                            {
                                //fire_engine.pposition = Flight_pos;
                                //fire_engine.UpdateParticles(0.5f);
                            }
                            

                            if (fl_width > 40 && fl_height > 55)
                            {
                                fl_width -= Gravity.X/2 * time ;
                                fl_height -= Gravity.Y/2 * time;
                                if (fl_width <= 100 && fl_height <= 125)
                                {
                                    if (te <= 10)
                                    {
                                        exp1.Play();
                                        explotion_engine.pposition = Flight_pos + new Vector2(-10, -10);
                                        explotion_engine.UpdateParticles(0.75f, 1.75f);
                                        te++;
                                    }
                                    else
                                        stop = true;
                                }
                                
                            }
                            

                        }
                        if (is_Attacked == true)
                        {
                            smokePos1 = new Vector2(Flight_pos.X + 75, Flight_pos.Y + 110);
                            smokePos1.X += rnd.Next(10) - 5;
                            smokePos1.Y += rnd.Next(10) - 5;
                            smokeList1.Add(smokePos1);
                        }
                        else
                        {
                            fl_takeoff.Play();
                        }

                        if (stop == true)
                        {
                            smokePos2 = new Vector2(Flight_pos.X + 90, Flight_pos.Y + 110);
                            smokePos2.X += rnd.Next(10) - 5;
                            smokePos2.Y += rnd.Next(10) - 5;
                            smokeList2.Add(smokePos2);
                            black+= 0.5f;
                        }
                        

            Missile_pos.Y -= Missile_velocity.Y * time;
            if (stop == false)
            {
                Flight_pos.Y -= Flight_velocity.Y * time;
            }
            Missile_rect = new Rectangle((int)Missile_pos.X,(int)Missile_pos.Y,Missile_tex.Width,Missile_tex.Height);

            Flight_rect = new Rectangle((int)Flight_pos.X, (int)Flight_pos.Y, (int)fl_width, (int)fl_height);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cam.getcamerapos());

            spriteBatch.Draw(bg, new Vector2(0, -3370), Color.White);

            if (stop == false)
            {
                spriteBatch.Draw(Flight_off, Flight_rect, Color.White);



                if (is_Attacked == true)
                {

                    spriteBatch.Draw(exp_animate, exp_pos, exp_rect, Color.White);
                    foreach (Vector2 smokePos1 in smokeList1)
                    {
                        spriteBatch.Draw(smokeTexture, smokePos1, null, Color.Gray, 0, new Vector2(40, 35), 0.5f, SpriteEffects.FlipVertically, 1);

                    }

                }

                explotion_engine.DrawParticles(spriteBatch);
            }
            else
            {
                foreach (Vector2 smokePos2 in smokeList2)
                {
                    spriteBatch.Draw(smokeTexture, smokePos2, null, Color.Gray, 0, new Vector2(40, 35), black, SpriteEffects.None, 1);
                }
            }
               if (is_Attacked == false && launch_mis == true)
                {
                    spriteBatch.Draw(Missile_tex, Missile_pos, Color.White);
                }
               //fire_engine.DrawParticles(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
