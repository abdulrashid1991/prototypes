﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace S4GP_Phyics
{
    public struct ParticleData
    {
        public float BirthTime;
        public float MaxAge;
        public Vector2 OrginalPosition;
        public Vector2 Accelaration;
        public Vector2 Direction;
        public Vector2 Position;
        public float Scaling;
        public Color ModColor;
    }
    

    class explotion
    {
        Texture2D explosion_tex;
        List<ParticleData> particleList = new List<ParticleData>();
        float age, maxage;

        public explotion(Texture2D tex)
        {
            explosion_tex = tex;
            age = 0;
        }

        public void AddExplosion(Vector2 explosionPos,Vector2 direction, int numberOfParticles, float size, float maxAge,float scale, GameTime gameTime)
        {

            maxage = maxAge;
            if (age < maxAge)
            {
                for (int i = 0; i < numberOfParticles; i++)
                {
                    AddExplosionParticle(explosionPos, size, maxAge, scale, direction, gameTime);
                    age ++;
                }

            }
        }

        public void AddExplosionParticle(Vector2 explosionPos, float explosionSize, float maxAge, float scale,Vector2 direction,GameTime gameTime)
        {
            ParticleData particle = new ParticleData();

            particle.OrginalPosition = explosionPos;
            particle.Position = particle.OrginalPosition;

            particle.BirthTime = (float)gameTime.TotalGameTime.TotalMilliseconds;
            particle.MaxAge = maxAge;
            particle.Scaling = scale;
            particle.ModColor = Color.White;

            float particleDistance = (float)Game1.rnd.NextDouble() * explosionSize;
            //Vector2 displacement = new Vector2(particleDistance, 0);
            //float angle = MathHelper.ToRadians(Game1.rnd.Next(360));
            //displacement = Vector2.Transform(displacement, Matrix.CreateRotationZ(angle));

            particle.Direction = direction;
            particle.Accelaration = 3.0f *particle.Direction;

            particleList.Add(particle);
        }

        public void draw(SpriteBatch sp)
        {
            if (age < maxage)
            {
                for (int i = 0; i < particleList.Count; i++)
                {
                    ParticleData particle = particleList[i];
                    sp.Draw(explosion_tex, particle.Position, null, particle.ModColor, 0, new Vector2(256, 256), particle.Scaling, SpriteEffects.None, 1);
                }
            }
        }

    }
}
