﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace S4GP_Phyics
{
    public class ParticleEngine
    {
        
        Texture2D ptexture;
        public Vector2 pposition;
        Vector2 pvelocity;

        Random random;
        List<Particle> particles;
        public static float exp_size;
       
       
        public ParticleEngine(Texture2D t)
        {
            ptexture = t;
            particles = new List<Particle>();
            random = new Random();
        }

        private Particle CreateParticles(float size1,float size2)
        {
            
            pvelocity = new Vector2(size2 * (float)(random.NextDouble() *2 -1),size2 * (float)(random.NextDouble() * 2-1));
            Particle newparticle = new Particle(ptexture,pposition,pvelocity,Color.White, size1, 30+random.Next(40));
            return newparticle; 
        }

        private void DeleteParticles()
        {
            for (int p = 0; p < particles.Count; p++)
            {
                particles[p].Update();
                if (particles[p].LifeTime <= 0)
                {
                    particles.RemoveAt(p);
                }
            }
        }
       

        public void UpdateParticles(float size1, float size2)
        {
            int total = 10;
            for (int p = 0; p < total; p++)
            {
                particles.Add(CreateParticles(size1,size2));
            }
            DeleteParticles();
       }

        public void DrawParticles(SpriteBatch spriteBatch)
        {
            
            for (int i = 0; i < particles.Count; i++)
            {
                particles[i].Draw(spriteBatch);
            }
            
        }


    }
}
