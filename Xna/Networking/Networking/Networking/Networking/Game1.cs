using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;

namespace Networking
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        connectivity con;
        KeyboardState ks;
        SpriteFont sf;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Components.Add(new GamerServicesComponent(this));
        }


        protected override void Initialize()
        {
 
            base.Initialize();
        }

       protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            con = new connectivity();
            sf = Content.Load<SpriteFont>("font");

        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            con.Refresh();
            ks = Keyboard.GetState();

            if (con.SessionState == NetworkSessionState.Lobby)
            {
                if (ks.IsKeyDown(Keys.F1))
                {
                    con.SignIn();
                }
                if (ks.IsKeyDown(Keys.F2))
                {
                    con.CreateSession();
                }

                if (ks.IsKeyDown(Keys.F3))
                {
                    con.FindSession();
                }
                if (ks.IsKeyDown(Keys.F4))
                {
                    con.JoinSession();
                }

                if (ks.IsKeyDown(Keys.F5))
                {
                    con.session.StartGame();
                }
            }
            
   
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            if (con.SessionState == NetworkSessionState.Lobby)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);
                spriteBatch.Begin();
                spriteBatch.DrawString(sf, con.msg, Vector2.Zero, Color.White);
                spriteBatch.End();
            }
            if (con.SessionState == NetworkSessionState.Playing)
            {
                GraphicsDevice.Clear(Color.Black);
            }

            base.Draw(gameTime);
        }
    }
}
