﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;

namespace Networking
{
    class connectivity
    {
        public NetworkSession session;
        public string msg = "Waiting...";

        AvailableNetworkSession found_session = null;
        AvailableNetworkSessionCollection all_sessions;

        public void FindSession()
        {
            all_sessions = NetworkSession.Find(NetworkSessionType.SystemLink, 1, null);
            foreach (AvailableNetworkSession cur_session in all_sessions)
            {
                int tot_slots = cur_session.OpenPublicGamerSlots + cur_session.OpenPrivateGamerSlots;
                if (tot_slots > cur_session.CurrentGamerCount)
                {
                    found_session = cur_session;
                }
            }

            if (found_session != null)
            {
                msg = found_session.HostGamertag + " hosted a session. press F4 to join.....";
            }
            else
            {
                msg = "No session found";
            }
        }

        public NetworkSessionState SessionState
        {
            get
            {
                if (session == null)
                    return NetworkSessionState.Lobby;
                else
                    return session.SessionState;
            }
        }
        public void JoinSession()
        {
            if (found_session != null)
            {
                session = NetworkSession.Join(found_session);
                if (session.IsHost == false)
                {
                    msg = "Congrats... u joined the session";
                }
            }
        }


        public void CreateSession()
        {
            if (session == null)
            {
                session = NetworkSession.Create(NetworkSessionType.SystemLink, 1, 3);
                session.GamerJoined += new EventHandler<GamerJoinedEventArgs>(gamer_joined);
                session.GamerLeft += new EventHandler<GamerLeftEventArgs>(gamer_left);

            }
        }

        public void gamer_joined(object sender, GamerJoinedEventArgs e)
        {
            if (e.Gamer.IsHost)
            {
                msg = e.Gamer.Gamertag + "has hosted the session";
            }
            else
            {
                msg = e.Gamer.Gamertag + "has joined the session  press F5 to start the game";
            }
        }

        public void gamer_left(object sender, GamerLeftEventArgs e)
        {
            msg = e.Gamer.Gamertag + "Has left the session";
        }

        public void SignIn()
        {
            if (!Guide.IsVisible)
            {
                Guide.ShowSignIn(1, false);
            }
        }

        public void Refresh()
        {
            if (session != null)
            {
                session.Update();
            }
        }
    }
}
