using UnityEngine;
using System.Collections;

namespace TheEscaper{
public class _Variables : MonoBehaviour
{

	public enum screenPOS{ top, topMid, mid, bot};
	public enum basketClass {frozen,moving,fliying};
	public enum basketPower {jetpack,freeze,life,noPower};
	
	public struct egg
	{
		Vector3 position;
		int currBasketID;
		bool isJumping;
		bool jumpReady;
		bool isLanded;
		bool isFliying;
		bool onTopBasket;
	};
	
	public struct basket
	{
		Vector3 position;
		screenPOS currPos;
		basketClass basketType;
		basketPower basketPower;
		bool hasEgg;
		bool isActive;
	};

}

}