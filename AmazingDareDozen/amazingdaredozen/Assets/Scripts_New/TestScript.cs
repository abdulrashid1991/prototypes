﻿using UnityEngine;
using System.Collections;

public class TestScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		NotificationCenter.DefaultCenter.AddObserver(this, "One");
	}

	void One()
	{
		Debug.Log("One");
	}
	// Update is called once per frame
	void Update () {
	
	}
}
