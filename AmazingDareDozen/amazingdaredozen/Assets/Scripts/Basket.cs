using UnityEngine;
using System.Collections;

public class Basket : MonoBehaviour {

	private float dir = 1;
	public float speed = Random.Range(1,3);
    private float multiplier = 0;
	void Awake() {
	
		transform.collider.enabled = false;
		dir = Random.Range(-1,1) >0 ? 1:-1;
	}
	

	void Update () {

		if(transform.position.x <= -44 || transform.position.x >= 44)
		{
			dir *= -1;
		}      
		transform.Translate(dir * speed* Time.deltaTime,0,0);
	}
	
	void DisableMovment()
	{
		dir = 0;
		speed = 0;
	}
	void Destroy()
	{
		Destroy(gameObject);
	}
		
	void EnableColl()
	{
		transform.collider.enabled = true;
	}
	
}
