using UnityEngine;
using System.Collections;

public class GUIStart : MonoBehaviour {

    public Texture2D bg;
    public GUISkin skin;
    private Rect buttonRect,screenRect;

    void Start()
    {
        buttonRect = new Rect(Screen.width / 2 - Screen.width / 4, Screen.height / 2 - Screen.height / 10, Screen.width / 2, Screen.height / 10);
        screenRect = new Rect(0, 0, Screen.width, Screen.height);
    }

    void Update()
    {
        
    }


	void OnGUI()
	{
        guiTexture.pixelInset = screenRect;
        if (Time.timeSinceLevelLoad < 2)
        {
            return;
        }
        else if (Time.timeSinceLevelLoad < 4)
        {
            guiTexture.color = Color.Lerp(guiTexture.color, new Color() { a = 0 }, 1 * Time.deltaTime);
            return;
        }
        else
        {
            GUI.skin = skin;
            if (GUI.Button(buttonRect, "Play"))
            {
                Application.LoadLevel("Game");
            }
        }
	}
}
