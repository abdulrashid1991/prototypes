using UnityEngine;
using System.Collections;


public class Egg : MonoBehaviour {
	public float magnitude;
	public float gravity;
	public Transform GameController;
	public int scoreIncr = 100;
	public Score score = new Score();
	
	private bool isJumping = false;
	private bool touch = false;
	private bool jump = false;
	private bool landed = true;
	private bool looping = false;
	private bool moveEgg = false;
	private bool scoreCalc = true;
	private bool inijump = true;
	private Transform currBasket;
	private Vector3 pos;
	private	float timeToLand;
	private	bool countStarted = false;

    public static int totalScore;

	

	
	void Start () {
		Physics.gravity = new Vector3 (0, gravity, 0);

	}


	void Update () {
		pos = transform.position;
		
		if(Input.GetMouseButtonDown(0) )
		{
			touch = true;
		}

		if(touch)
		{
			jump = true;
			touch = false;
		}
		
		if(jump)
		{
			Jump();
			jump = false;
		}
		
		if(moveEgg)
		{
			pos.x = currBasket.position.x;
			transform.position = pos;
		} 
	}
	
	/*public void OnGUI(){
		scoreGUI.anchor = TextAnchor.UpperRight;
		scoreGUI.fontSize = 10;
		scoreGUI.pixelOffset = new Vector2(200,400);
		scoreGUI.text = "Score: " + totalScore.ToString(); 
		
	}*/
	
	
	

	
	public bool Jump()
	{
		if(!looping)
		{
			if(!isJumping)
			{
			rigidbody.AddRelativeForce(Vector3.up * magnitude);
			GameController.SendMessage("DisableCurrBasket");
			isJumping = true;
			landed = false;
			inijump = false;
			return true;
			}
		}
		return false;
	}
		
	void OnCollisionEnter(Collision collisioninfo){
		landed = true;
		isJumping = false;
		ScoreCalc();
	}
	void OnCollisionStay(Collision collisioninfo){
		currBasket = collisioninfo.transform;
		GameController.SendMessage("GetCurrentBasket", currBasket);
		moveEgg = true;
	}
	
	void OnCollisionExit(Collision collisioninfo){
		GameController.SendMessage("EnableBasket");
		moveEgg = false;
		scoreCalc = true;
		//landed = false;
	}
	
	bool IsLooping(bool val)
	{
		looping = val;
		return looping;
	}
	
	void ScoreCalc()
	{
		if(scoreCalc)
		{
			if(!inijump){
			//Score.totalScore+= scoreIncr;
            totalScore += scoreIncr;
			scoreCalc = false;
			}
		}
	}
	

}
