 using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
	
	public Transform egg;
	public List<Transform> basketlist;
	public Transform basket;
	public Vector3 initialPos;
	public float distance;
	
	private Vector3 currentPos;
	private Transform currBasket;
	private Transform nextBasket;
	private bool isJumping = false;
	private bool isLooping = false;
	private bool moveScreen = false;
	private int currBasketId = 0;

	
	void Awake()
	{
		currentPos = initialPos;
		basketlist.Add((Transform)Instantiate(basket,initialPos,Quaternion.Euler(new Vector3(0,180,0))));
		basketlist[0].SendMessage("EnableColl");    
		basketlist[0].SendMessage("DisableMovment");
		basketlist.Add((Transform)Instantiate(basket,GetNextPosition(),Quaternion.Euler(new Vector3(0,180,0))));
		basketlist.Add((Transform)Instantiate(basket,GetNextPosition(),Quaternion.Euler(new Vector3(0,180,0))));	
	}
	
	void Update () {
		
		CheckGO();
		if(isJumping && !isLooping)
		{
			if(egg.position.y > nextBasket.position.y+10)
			{
				currBasket.collider.enabled = false;
				nextBasket.collider.enabled = true;
			}
		}
		
		if(moveScreen)
		{
			if(MoveScreen())
			{
				moveScreen = false;
				DeleteCurrentPage();
			}
		}
		
	}
	
	void EnableBasket()
	{
		isJumping = true;
	}
	
	bool GetCurrentBasket(Transform basketin){
	
		for(int i =0; i < basketlist.Count ; i++ )
		{
			if(basketlist[i] == basketin){
				if( i != 2 )
				{
					currBasket = basketin;
					currBasketId = i;
					nextBasket = basketlist[currBasketId + 1];
				}
				else 
				{
					if(!isLooping)
					{
						InitNextPage();
						moveScreen = true;
					}
				}
			}
		}
		return false;
	}
	
	public Vector3 GetNextPosition()
	{
		currentPos.y +=distance;
		currentPos.x =Random.Range(-40,40);
		return currentPos;
	}
	
	public bool InitNextPage()
	{
		isLooping = true;
		basketlist.Add((Transform)Instantiate(basket,GetNextPosition(),Quaternion.Euler(new Vector3(0,180,0))));
		basketlist.Add((Transform)Instantiate(basket,GetNextPosition(),Quaternion.Euler(new Vector3(0,180,0))));			
		return true;
	}
	
	public bool MoveScreen()
	{
		bool moved;
		if(basketlist[0].position.y >= initialPos.y - (distance*2))
		{
			basketlist[0].transform.Translate(0,-1,0);
			moved = false;
		}
		else
			moved = true;
		
		if(basketlist[1].position.y >= initialPos.y - distance)
		{
			basketlist[1].transform.Translate(0,-1,0);
			moved = false;
		}
		else
			moved = true;
		
		if(basketlist[2].position.y >= initialPos.y )
		{
			basketlist[2].transform.Translate(0,-1,0);
			moved = false;
		}
		else
		{
			moved = true;
		}
		
		if(basketlist[3].position.y >= initialPos.y + distance)
		{
			basketlist[3].transform.Translate(0,-1,0);
			moved = false;
		}
		else
		{
			moved = true;
		}
		
		if(basketlist[4].position.y >= initialPos.y + (distance*2))
		{
			basketlist[4].transform.Translate(0,-1,0);
			moved = false;
		}
		else
		{
			moved = true;
		}
		
		if(egg.position.y >= -70.44)
		{
			egg.transform.Translate(0,-1,0);
			moved = false;
		}
		else
		{
			moved = true;
		}
				
		if(moved)
		{
			egg.SendMessage("IsLooping",false);
			return true;
		}
		else
		{
			egg.SendMessage("IsLooping",true);
			return false;
		}
		
	}
	
	public bool DeleteCurrentPage()
	{
		basketlist[0].SendMessage("Destroy");
		basketlist[1].SendMessage("Destroy");
		basketlist.RemoveAt(1);
		basketlist.RemoveAt(0);
		currentPos = new Vector3(initialPos.x +Random.Range(-50.0f,50.0f),initialPos.y +(distance * 2), initialPos.z);
		isLooping = false;
		return true;
	}
	//+Random.Range(-50.0f,50.0f)
	
	void CheckGO()
	{
		if(egg.position.y < currBasket.position.y-10)
		{
			Debug.Log("GameOver");
			Application.LoadLevel("GameOver");
		}
	}

    void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 100, 100), "Time" + Mathf.FloorToInt(Time.timeSinceLevelLoad).ToString());
        GUI.Label(new Rect(Screen.width - 80, 0, 100, 100), "Score :  " + Egg.totalScore.ToString());
    }
	

	
		
}
