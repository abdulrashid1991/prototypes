﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

    public GUISkin skin;

    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        guiTexture.pixelInset = new Rect(-Screen.width/2,-Screen.height/2 , Screen.width , Screen.height );
	}

    void OnGUI()
    {
        GUI.skin = skin;
        Rect menu = GUI.Window(0, new Rect(Screen.width / 2 - Screen.width / 4, Screen.height / 2 + Screen.height / 6, Screen.width / 2, Screen.height / 4), MenuFunc, "");
    }

    private void MenuFunc(int id)
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Exit"))
        {
            Application.Quit();
        }
        if (GUILayout.Button("Restart"))
        {
            Application.LoadLevel("Game");
        }
        GUILayout.EndArea();
    }
}
