﻿using UnityEngine;
using System.Collections;

public class FadeOnLoad : MonoBehaviour {

    public float time;

    void OnGUI()
    {
        if (Time.timeSinceLevelLoad < time)
            guiTexture.color = Color.Lerp(guiTexture.color, Color.clear, 1 * Time.deltaTime);
        else
            guiTexture.enabled = false;
    }
}
