﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Difficulty { Easy, Medium, Hard, None };
public enum Operation {Addition,Subraction,Division,Multiplication,None};
public enum GameState { Lobby,GamePlay,Finished,Results,None };
public enum MenuState { SplashScreen,GameIntro,PlayerMenu,ToGame,CountDown,None};
public struct PlayerStruct
{
    public int id;
    public string name;
    public Transform transform;
    public bool isStarted;
    public bool isFinished;
    public float finishedTime;
    public Texture2D image;
}

public class Variables : MonoBehaviour
{
    public static string PlayerName;
    public static Difficulty level;
    public static Operation operation;
    public static bool isGameStarted;
    public static GameState gameState;
    public static MenuState Menustate;
    public static List<PlayerStruct> players;
    public static List<PlayerStruct> finishedPlayers;
    public static Stats stats;
    public static float gameStartTime;

    public static IEnumerator LoadLevel(string name, float time)
    {
        Debug.Log("Waiting");
        yield return new WaitForSeconds(time);
        Debug.Log("after 2 sec");
        Application.LoadLevel(name);
    }
}

public struct QuestionStruct
{
    public int xValue;
    public int yValue;
    public int answer;
    public int[] choices;
}

[System.Serializable]
public class GUIData
{
    public GUIText name;
    public GUIText time;
    public GUITexture texture;
}

public struct Stats
{
    public int correctAns;
    public int wrongAns;
    public int totalQues;
    public float totalTime;
    public List<QuestionStruct> missedQuest;

}