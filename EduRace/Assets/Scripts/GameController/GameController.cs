﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public Texture2D[] countDown;
    public Rect countDownRect = new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 200);
    public List<AICarScript> aiCars;
    public AICarScript player;
    public Texture2D startSkin;
    public GUIStyle noGuisStyle;

    private bool startGame = false;
    private float startTime;

    void Start()
    {
        noGuisStyle = new GUIStyle();
        if (Variables.players == null)
            Variables.players = new List<PlayerStruct>();
        if (Variables.finishedPlayers == null)
            Variables.finishedPlayers = new List<PlayerStruct>();
        Controler tempcon = player.GetComponent<Controler>();
        PlayerStruct temp = new PlayerStruct() { isFinished = false, isStarted = false, name = Variables.PlayerName, transform = player.transform ,id = tempcon.carId,image = tempcon.tumbImage};
        Variables.players.Add(temp);
        foreach (var item in aiCars)
        {
            tempcon = item.GetComponent<Controler>();
            temp = new PlayerStruct() { isFinished = false,  transform = item.transform, name = "Computer" + item.GetComponent<Controler>().carId, isStarted = false,id = tempcon.carId ,image = tempcon.tumbImage };
            Variables.players.Add(temp);
        }
    }

    Color temp;
    void Update()
    {
        guiTexture.enabled = true;
        guiTexture.color = Color.Lerp(Color.clear, Color.black, 1 * Time.deltaTime);
        if(Variables.players.Count == Variables.finishedPlayers.Count)
        {
            Variables.gameState = GameState.Finished;
            Variables.stats.totalTime = Variables.gameStartTime - Time.timeSinceLevelLoad;
            StartCoroutine( Variables.LoadLevel("Result", 2));
        }
    }
    void OnGUI()
    {
        if (Variables.gameState == GameState.Lobby)
        {
            //GUI.skin = startSkin;
            if (GUI.Button(new Rect(Screen.width/2 - 100,Screen.height/2 - 100,200,200), startSkin,noGuisStyle))
            {
                startGame = true;
                startTime = Time.timeSinceLevelLoad;
                Variables.gameStartTime = Time.timeSinceLevelLoad;
            }
        }

        if (startGame)
        {
            //if (startTime + 1 > Time.timeSinceLevelLoad)
            //    GUI.DrawTexture(countDownRect, countDown[0]);
            //else if (startTime + 2 > Time.timeSinceLevelLoad)
            //    GUI.DrawTexture(countDownRect, countDown[1]);
            //else if (startTime + 3 > Time.timeSinceLevelLoad)
            //    GUI.DrawTexture(countDownRect, countDown[2]);
            if (startTime + 3 > Time.timeSinceLevelLoad)
            {
                Variables.gameState = GameState.GamePlay;
                foreach (var item in aiCars)
                {
                    item.GamePlay(true);
                }
            }
            
        }
    }

}
