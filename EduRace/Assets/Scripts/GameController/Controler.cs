﻿using UnityEngine;
using System.Collections;

public class Controler : MonoBehaviour {
    public int carId;
    public bool isFinished = false;
	public bool isPlayer = false;
    public Texture2D tumbImage;

    private PlayerStruct finishedPlayer;
	private int rnd;
	private bool boost;
	public void LateUpdate()
	{
        if (Time.timeSinceLevelLoad < 1)
            return;

		if (boost) {
			rnd = Random.Range (0, 4);
			if (!isPlayer) {
				switch (rnd) {
				case 0:
						{
								transform.SendMessage ("Boost");
								boost = false;
						}
						break;
				case 1:
						{
								transform.SendMessage ("Brake");
								boost = false;
						}
						break;
				default:
						break;
				}

			}
		}
	else {
		StartCoroutine("Wait");
		}
	}
	IEnumerator Wait()
	{
		yield return new WaitForSeconds (1);
		boost = true;
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish")
        {
            isFinished = true;
            for (int i = 0; i < Variables.players.Count; i++)
            {
                if (Variables.players[i].id == carId)
                {
                    PlayerStruct temp = Variables.players[i];
                    temp.isFinished = true;
                    temp.finishedTime = Time.timeSinceLevelLoad;
                    Variables.players[i] = temp;
                    finishedPlayer = Variables.players[i];
                    GetComponent<AICarScript>().HaltCar();
                }
            }
            if (!Variables.finishedPlayers.Exists((x) => (x.id == finishedPlayer.id)))
            {
                Variables.finishedPlayers.Add(finishedPlayer);
            }
        }
    }

}
