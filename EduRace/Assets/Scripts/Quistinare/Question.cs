﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Question : MonoBehaviour {

    public Rect questionRect;
    public Difficulty level;
    public Operation operation;
    public AICarScript Player;

    public GUIText questNo;
    public GUIText playerName;
    public GUISkin questSkin;
    private QuestionStruct currQuestion;
    private int questionIndex = 1;
    private bool isQuestGenerated = false;
    void Start()
    {
        //Variables.level = level;
        //Variables.operation = operation;
        playerName.text = Variables.PlayerName;
    }

    void Update()
    {
        if (!isQuestGenerated)
        {
            currQuestion = GenNextQues();
            isQuestGenerated = true;
        }
        if (Variables.gameState == GameState.GamePlay)
        {
            Player.GamePlay(true);
        }
        questNo.text = questionIndex.ToString();
    }

    private QuestionStruct GenNextQues()
    {
        int xMin = 0, xMax = 0, yMin = 0, yMax = 0;

        switch (Variables.level)
        {
            case Difficulty.Easy:
                {
                    xMin = 1; xMax = 5;
                    yMin = 1; yMax = 5;
                }
                break;
            case Difficulty.Medium:
                {
                    xMin = 1; xMax = 5;
                    yMin = 1; yMax = 10;
                }
                break;
            case Difficulty.Hard:
                {
                    xMin = 1; xMax = 10;
                    yMin = 5; yMax = 15;
                }
                break;
        }
        QuestionStruct question = new QuestionStruct();
        question.choices = new int[4];

        GenerateRandom:
        int choice = Random.Range(0, 3);
        question.xValue = Random.Range(xMin, xMax);
        question.yValue = Random.Range(yMin, yMax);
        switch (Variables.operation)
        {
            case Operation.Addition:
                {
                    question.answer = question.xValue + question.yValue;
                    List<int> tempChoices = new List<int>(4);
                    for (int i = 0; i < 4; i++)
                    {
                        if (i == choice)
                            tempChoices.Add(question.answer);
                        else
                        {
                        RegenAdd:
                            int temp = Random.Range(question.answer - xMax, question.answer + xMax);
                            if(temp < 0)
                                 temp = Random.Range(question.answer, question.answer + 5);
                            if (!tempChoices.Contains(temp))
                                tempChoices.Add(temp);
                            else
                                goto RegenAdd;
                        }
                    }
                    question.choices = tempChoices.ToArray();
                }
                break;
            case Operation.Subraction:
                {
                    if (question.xValue > question.yValue)
                    {
                        question.answer = question.xValue - question.yValue;
                        List<int> tempChoices = new List<int>(4);
                        for (int i = 0; i < 4; i++)
                        {
                            if (i == choice)
                               tempChoices.Add(question.answer);
                            else
                            {
                            RegenSub:
                                int temp = Random.Range(question.answer - xMax, question.answer + xMax);
                                if (temp < 0)
                                    temp = Random.Range(question.answer, question.answer + xMax);
                                if (!tempChoices.Contains(temp))
                                    tempChoices.Add(temp);
                                else
                                    goto RegenSub;
                            }
                        }
                        question.choices = tempChoices.ToArray();
                    }
                    else
                        goto GenerateRandom;
                }
                break;
            case Operation.Division:
                {
                    int ans = Random.Range(xMin, xMax);
                    question.xValue = question.yValue * ans;
                    question.answer = ans;
                    List<int> tempChoices = new List<int>(4);
                    for (int i = 0; i < 4; i++)
                    {
                        if (i == choice)
                            tempChoices.Add(question.answer);
                        else
                        {
                        ReGenDiv:
                            int temp = Random.Range(xMin, xMax);
                            if (!tempChoices.Contains(temp))
                                tempChoices.Add(temp);
                            else
                                goto ReGenDiv;
                        }
                    }
                    question.choices = tempChoices.ToArray();
                }
                break;
            case Operation.Multiplication:
                question.answer = question.xValue * question.yValue;
                List<int> tempChoices1 = new List<int>(4);
                        for (int i = 0; i < 4; i++)
                        {
                            if (i == choice)
                                tempChoices1.Add(question.answer);
                            else
                            {
                            RegenMul:
                                int temp = Random.Range(xMin, xMax) * Random.Range(yMin, yMax);
                                if (!tempChoices1.Contains(temp))
                                    tempChoices1.Add(temp);
                                else
                                    goto RegenMul;
                            }
                        }
                        question.choices = tempChoices1.ToArray();
                break;
        }




        return question;

    }

    void OnGUI()
    {
        GUI.skin = questSkin;
        if (Variables.gameState == GameState.GamePlay)
        {
            Rect quest = GUI.Window(0, questionRect, QuestionWin, "");
        }
    }

    void QuestionWin(int id)
    {
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        GUILayout.Space(50);
        GUILayout.Box(currQuestion.xValue.ToString());
        switch (Variables.operation)
        {
            case Operation.Addition:
                GUILayout.Box("+");
                break;
            case Operation.Subraction:
                GUILayout.Box("-");
                break;
            case Operation.Division:
                GUILayout.Box("/");
                break;
            case Operation.Multiplication:
                GUILayout.Box("*");
                break;
        }
        GUILayout.Box(currQuestion.yValue.ToString());
        GUILayout.EndHorizontal();
        //GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        foreach (var item in currQuestion.choices)
        {

            if (GUILayout.Button(item.ToString(),GUILayout.Height(40)))
            {
                if (item == currQuestion.answer)
                {
                    currQuestion = GenNextQues();
                    Player.Boost();
                    Variables.stats.correctAns++;
                    Debug.Log("Correct");
                }
                else
                {
                    currQuestion = GenNextQues();
                    Player.Brake(); Variables.stats.wrongAns++;
                    Debug.Log("Incorrect");
                    if (Variables.stats.missedQuest == null)
                        Variables.stats.missedQuest = new List<QuestionStruct>();
                    Variables.stats.missedQuest.Add(currQuestion);
                }
                Variables.stats.totalQues++;
                questionIndex++;

            }

        }
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
    }




}
