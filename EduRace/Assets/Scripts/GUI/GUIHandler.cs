﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIHandler : MonoBehaviour {

    public GUISkin startSkin, menuSkin, DropDown;
    public Camera mainCamera;
    string defName;
     
    string name;

    void Awake()
    {
        Variables.gameState = GameState.Lobby;
        Variables.Menustate = MenuState.SplashScreen; 
        //name = "Player" + Random.Range(222, 999);
        mainCamera.animation.Stop();
        mainCamera.animation.clip.frameRate = 5;
    }

    void Start()
    {
        defName =  "Player" + Random.Range(222, 999);
        name = defName;
    }

    void Update()
    {
        if (Time.timeSinceLevelLoad < 3)
            return;
        else if (Time.timeSinceLevelLoad < 5)
        {
            guiTexture.color = Color.Lerp(guiTexture.color, new Color() { a = 0 }, 1 * Time.deltaTime);
            Variables.Menustate = MenuState.PlayerMenu;
            return;
        }
        else
        {
            //guiTexture.active = false;
            //Variables.Menustate = MenuState.GameIntro;
            if (!mainCamera.animation.isPlaying)
                mainCamera.animation.Play();
        }

        if (Variables.Menustate == MenuState.PlayerMenu)
            mainCamera.animation["camera"].speed = 0;
    }

    void OnGUI()
    {
        if (Time.timeSinceLevelLoad < 2)
            return;
        switch (Variables.Menustate)
        {

            case MenuState.GameIntro:
                GUI.skin = startSkin;
                if (GUI.Button(new Rect(Screen.width - 150, Screen.height - 150, 100, 100), ""))
                {
                    Variables.Menustate = MenuState.CountDown;
                    mainCamera.animation["camera"].speed = 0;
                    //mainCamera.animation.Stop();
                }
                break;
            case MenuState.CountDown:
                {
                    Variables.Menustate = MenuState.PlayerMenu;
                }break;
            case MenuState.PlayerMenu:
                {
                    GUI.skin = menuSkin;
                    Rect PlayerName = GUI.Window(0, new Rect(Screen.width / 2 - Screen.width / 6, Screen.height / 2 - Screen.height / 4, Screen.width / 3, Screen.height / 2), MenuWin, ""); 
                }
                break;
        }
        
    }



    bool isDiffClicked = false,isOpClicked = false,isClicked;
    Vector2 scrollPosOp, scrollPosdiff;

    void MenuWin(int id)
    {

        GUILayout.BeginVertical();
        GUI.SetNextControlName("Player_Name");
        name = GUILayout.TextField(name,GUILayout.Height(40));


        if (GUI.GetNameOfFocusedControl() == "Player_Name")
        {
            if (name == defName)
                name = "";
        }
        else
            if (name == "")
                name = defName;


        if (GUILayout.Button(Variables.operation == Operation.None ? "Operation" : Variables.operation.ToString(),GUILayout.Height(40)))
        {
            if (!isOpClicked)
                isOpClicked = true;
            else
                isOpClicked = false;
            isDiffClicked = false;
        }
        
        if (isOpClicked)
        {
            scrollPosOp = GUILayout.BeginScrollView(scrollPosOp, false, true);
            GUI.skin = DropDown;
            if (GUILayout.Button(Operation.Addition.ToString(), GUILayout.Height(30)))
            {
                Variables.operation = Operation.Addition;
                isOpClicked = false;
            }
            if (GUILayout.Button(Operation.Subraction.ToString(), GUILayout.Height(30)))
            {
                Variables.operation = Operation.Subraction;
                isOpClicked = false;
            }
            if (GUILayout.Button(Operation.Multiplication.ToString(), GUILayout.Height(30)))
            {
                Variables.operation = Operation.Multiplication;
                isOpClicked = false;
            } if (GUILayout.Button(Operation.Division.ToString(), GUILayout.Height(30)))
            {
                Variables.operation = Operation.Division;
                isOpClicked = false;
            }
            GUILayout.EndScrollView();
        }
        GUI.skin = menuSkin;
        if (GUILayout.Button(Variables.level == Difficulty.None ? "Level" : Variables.level.ToString(), GUILayout.Height(40)))
        {
            if (!isDiffClicked)
                isDiffClicked = true;
            else
                isDiffClicked = false;
            isOpClicked = false;
        }
        if (isDiffClicked)
        {
            GUI.skin = DropDown;
            if (GUILayout.Button(Difficulty.Easy.ToString(), GUILayout.Height(30)))
            {
                Variables.level = Difficulty.Easy;
                isDiffClicked = false;
            }
            if (GUILayout.Button(Difficulty.Medium.ToString(), GUILayout.Height(30)))
            {
                Variables.level = Difficulty.Medium;
                isDiffClicked = false;
            }
            if (GUILayout.Button(Difficulty.Hard.ToString(), GUILayout.Height(30)))
            {
                Variables.level = Difficulty.Hard;
                isDiffClicked = false;
            }
        }

        GUI.skin = menuSkin;
        if (GUILayout.Button("Continue",GUILayout.Height(40)))
        {
            Variables.PlayerName = name;
            StartCoroutine(Variables.LoadLevel("Game", 0));
        }

        GUILayout.EndVertical();
    }

}
