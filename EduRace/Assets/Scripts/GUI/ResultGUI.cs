﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ResultGUI : MonoBehaviour {

    public List<GUIData> guiData;
    public GUIText accuracyImg, rateImg;
    public Texture2D fadeImg;
    public Rect missedRect,printRect,continueRect,homeRect;
    public GUISkin skin;
    
    private GUIStyle noGuiStyle;
    
	void Start () {
        //temp
        //Variables.stats.correctAns = 5;
        //Variables.stats.wrongAns = 5;
        //Variables.stats.totalQues = 10;
        //Variables.stats.totalTime = 254;
        //Variables.stats.missedQuest = new List<QuestionStruct>();
        //for (int i = 0; i < 50; i++)
        //{
        //    Variables.stats.missedQuest.Add(new QuestionStruct() { xValue = i * 2, yValue = i * 2, answer = i * 4 });
        //}
        //Variables.operation = Operation.Addition;
        //
        float c = Variables.stats.correctAns;
        float t = Variables.stats.totalQues;
        float accuracy = (c/t) * 100;
        int rate = -(int)(Variables.stats.totalQues / Variables.stats.totalTime * 60) ;
        accuracyImg.text = System.Math.Round(accuracy,2).ToString() + "%";
        rateImg.text = Mathf.Round(rate).ToString() + " /min";

        for (int i = 0; i < Variables.finishedPlayers.Count; i++)
        {
            guiData[i].name.text = Variables.finishedPlayers[i].name;
			float temp = (Variables.finishedPlayers[i].finishedTime - Variables.gameStartTime) ;
			guiData[i].time.text = System.Math.Round(temp,1).ToString()  + " Sec";
            guiData[i].texture.texture = Variables.finishedPlayers[i].image;
        }
	}

    void OnGUI()
    {
        GUI.skin = skin;
        if (Time.timeSinceLevelLoad < 1)
            return;
        if (GUI.Button(printRect, "PrintTrophy"))
        {
            
        }
        if (GUI.Button(continueRect, "Continue"))
        {
            StartCoroutine(Variables.LoadLevel("Menu",3f));
        }
        if (GUI.Button(homeRect, "Home"))
        {
            Application.OpenURL("http://www.animantra.in");
        }
        Rect questRect = GUI.Window(0, missedRect, MissedQuest,"");
    }

    private Vector2 scrollpos;
    private void MissedQuest(int id)
    {
        GUI.skin = skin;
        GUILayout.BeginVertical();
        scrollpos = GUILayout.BeginScrollView(scrollpos,false,true);
        foreach (var item in Variables.stats.missedQuest)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Box(item.xValue.ToString(),GUILayout.Width(30));
            GUILayout.Space(20);
            switch (Variables.operation)
            {
                case Operation.Addition:
                    GUILayout.Box("+", GUILayout.Width(30));
                    break;
                case Operation.Subraction:
                    GUILayout.Box("-", GUILayout.Width(30));
                    break;
                case Operation.Division:
                    GUILayout.Box("/", GUILayout.Width(30));
                    break;
                case Operation.Multiplication:
                    GUILayout.Box("*", GUILayout.Width(30));
                    break;
            }
            GUILayout.Box(item.yValue.ToString(), GUILayout.Width(30));
            GUILayout.Space(20);
            GUILayout.Box("=", GUILayout.Width(30));
            GUILayout.Space(20);
            GUILayout.Box(item.answer.ToString(), GUILayout.Width(30));
            GUILayout.EndVertical();

        }
        GUILayout.EndScrollView();
        GUILayout.EndVertical();
    }
	

}
