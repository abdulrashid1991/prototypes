@script AddComponentMenu ("CarPhys/AIScripts/AICarScript")
var centerOfMass : Vector3;  
var path : Array;  
var pathGroup : Transform;  
var maxSteer : float = 15.0;  
var wheelFL : WheelCollider;   
var wheelFR : WheelCollider;  
var wheelRL : WheelCollider;   
var wheelRR : WheelCollider;  
var currentPathObj : int;  
var distFromPath : float = 20;  
var maxTorque : float = 50;  
var currentSpeed : float;  
var topSpeed : float = 150;  
var decellarationSpeed : float = 10;  
//var gearRatio : int[];	
var breakingMesh : Renderer;  
var idleBreakLight : Material;  
var activeBreakLight : Material;  
var isBreaking : boolean;  
var inSector : boolean;  
var isControll = false;
var changeTime : float;
var isGamePlay : boolean;
var boostTime : float;
var particleEffectLeft : GameObject;
var particleEffectRight : GameObject;
var isPlayer : boolean;

private var haltCar = false;
function Start () {  
    if(isPlayer)
    {
        particleEffectLeft.GetComponent(ParticleEmitter).emit = false;
        particleEffectRight.GetComponent(ParticleEmitter).emit = false;
        particleEffectLeft.GetComponent(LensFlare).enabled = false;
        particleEffectRight.GetComponent(LensFlare).enabled = false;
    }
	rigidbody.centerOfMass = centerOfMass;  
	GetPath();  
	isGamePlay = false;
}  
  
function GetPath (){  
	var path_objs : Array = pathGroup.GetComponentsInChildren(Transform);  
	path = new Array();  
	  
	for (var path_obj : Transform in path_objs){  
	  path.Add(path_obj);  
	}  
}  
    
      
function Update () {  

	if(!isGamePlay)
	    return;

	if(changeTime < Time.timeSinceLevelLoad)
	{
	    maxTorque = 30;
	    topSpeed = 150;
		isBreaking = false;
		if(isPlayer)
		{
		    particleEffectLeft.GetComponent(ParticleEmitter).emit = false;
		    particleEffectRight.GetComponent(ParticleEmitter).emit = false;
		    particleEffectLeft.GetComponent(LensFlare).enabled = false;
		    particleEffectRight.GetComponent(LensFlare).enabled = false;
		    wheelFL.transform.GetComponent(SkiddingScript).StopBrake();
		    wheelFR.transform.GetComponent(SkiddingScript).StopBrake();
		    wheelRL.transform.GetComponent(SkiddingScript).StopBrake();
		    wheelRR.transform.GetComponent(SkiddingScript).StopBrake();
		}
	}
	GetSteer(); 
	if(isControll){
		Move(); 
	}
	BreakingEffect ();  

}  
 
public function GamePlay(isPlaying:boolean)
	{
	    isGamePlay = isPlaying;
	}

public function HaltCar()
	{
        haltCar = true;

	}
	
public function Boost()
	{
		maxTorque = 100;
		topSpeed = 250;
		changeTime = Time.timeSinceLevelLoad + boostTime;
		if(isPlayer)
		{
		    particleEffectLeft.GetComponent(ParticleEmitter).emit = true;
		    particleEffectRight.GetComponent(ParticleEmitter).emit = true;
		    particleEffectRight.GetComponent(LensFlare).enabled = true;
		    particleEffectLeft.GetComponent(LensFlare).enabled = true;

		}

	}
public function Brake()
	{
        maxTorque = 15;
        topSpeed = 50;
		isBreaking = true;
		changeTime = Time.timeSinceLevelLoad + boostTime;
		if(isPlayer)
		{
		    wheelFL.transform.GetComponent(SkiddingScript).Brake();
		    wheelFR.transform.GetComponent(SkiddingScript).Brake();
		    wheelRL.transform.GetComponent(SkiddingScript).Brake();
		    wheelRR.transform.GetComponent(SkiddingScript).Brake();
		}
	}
  
function GetSteer(){  
    var inUse: Transform; inUse = path[currentPathObj]as Transform;
 
	var steerVector : Vector3 = transform.InverseTransformPoint(Vector3(inUse.position.x, inUse.position.y,inUse.position.z));
	var newSteer : float = maxSteer * (steerVector.x / steerVector.magnitude);  
	wheelFL.steerAngle = newSteer;  
	wheelFR.steerAngle = newSteer;  

	if (steerVector.magnitude <= distFromPath){  
	    if (currentPathObj <= path.length )
	        currentPathObj++;
	}  

	//if (currentPathObj >= path.length ){  
	//	currentPathObj = 0; 
	//} 


}  
  
function Move (){  
	currentSpeed = 2*(22/7)*wheelRL.radius*wheelRL.rpm * 60 / 1000;  
	currentSpeed = Mathf.Round (currentSpeed);  
	if (currentSpeed <= topSpeed && !inSector){  
		wheelRL.motorTorque = maxTorque;  
		wheelRR.motorTorque = maxTorque;  
		wheelRL.brakeTorque = 0;  
		wheelRR.brakeTorque = 0;  
	}  
	else if (!inSector){  
		wheelRL.motorTorque = 0;  
		wheelRR.motorTorque = 0;  
		wheelRL.brakeTorque = decellarationSpeed;  
		wheelRR.brakeTorque = decellarationSpeed;  
	}  
}  
  
function BreakingEffect (){  
	if (isBreaking){  
		breakingMesh.material = activeBreakLight;  
	}  
	else {  
		breakingMesh.material = idleBreakLight;  
	}  
}
  