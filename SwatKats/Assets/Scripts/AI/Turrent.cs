using UnityEngine;
using System.Collections;

public class Turrent : MonoBehaviour {
	
	public Transform target;
	public Transform muzzlePTPivot;
	public Transform leftMuzzlePT;
	public Transform rightMuzzlePT;	
	public Transform bullet;
	public float activeDistance;
	public float timeDelay ;
	public Transform explosion;
	
	bool test = true;
	private Ray ray;
	private RaycastHit hitInfo;
	public Vector3  turrentAngle;
	private float gameTime ;
	private float tempTime = 0.0f;
	
	void Awake(){
	}
	
	void Start () {
		target = (Transform)GameObject.FindWithTag("Player").GetComponent("Transform");
	}
	
	

	void FixedUpdate () {
        if (Variables.isGameOver)
        {
            return;
        }
		gameTime = Time.time;
		transform.LookAt(target);
		Vector3 angle = (target.position - transform.position).normalized ;
		Debug.DrawRay(transform.position,angle * 100 ,Color.red);
		ray = new Ray(transform.position, angle);
		
		if(Physics.Raycast(ray, out hitInfo) && CalcTime())
		{ 
			if(hitInfo.transform == target)
			{
				if(Vector3.Distance(transform.position,hitInfo.point) < activeDistance)
				{
					FireBullet(hitInfo);
				}
			}
	
		}
	}
	
	
	public bool FireBullet(RaycastHit hit)
	{
		Transform lBullet =  (Transform)Instantiate(bullet);
		lBullet.position = leftMuzzlePT.position;
		//lBullet.LookAt(target);	

		Transform rBullet =  (Transform)Instantiate(bullet);
		rBullet.position = rightMuzzlePT.position;
		//rBullet.LookAt(target);
		
		return true;
	}
	
	public bool CalcTime()
	{	
		if(gameTime > tempTime)
		{
			tempTime = gameTime + timeDelay;
			return true;
		}
		return false;
	}
	
	public void DestroyGO(){
		Instantiate(explosion,transform.position,transform.rotation);
		Destroy(gameObject,0.5f);
	}
}
