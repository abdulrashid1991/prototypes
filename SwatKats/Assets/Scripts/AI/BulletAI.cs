using UnityEngine;
using System.Collections;

public class BulletAI : MonoBehaviour {
	public float speed = 1;
	public Transform explosion;
	public Transform target;
	public float damp = 10;
	public Rigidbody rigidbody;
	

	void Start () {
		Transform missile = transform;
		Transform turrent = (Transform)GameObject.FindWithTag("Turrent").GetComponent("Transform");
		target = (Transform)GameObject.FindWithTag("Player").GetComponent("Transform");
		rigidbody = (Rigidbody)GetComponent(typeof(Rigidbody));
	}
	
	void Update () {
		transform.LookAt(target);
		Vector3 targetPos = Vector3.MoveTowards(transform.position,target.position,speed);
		transform.position = Vector3.Lerp(transform.position, targetPos, damp);
		//transform.Translate(Vector3.forward * speed * Time.deltaTime ,Space.Self);
		Destroy(gameObject,5);

	}
	
	
	void OnCollisionEnter(Collision collision)
	{
		if(collision.transform.tag == "Player")
		{
			Instantiate(explosion,transform.position,transform.rotation);
			Destroy(gameObject);
		}
	}
	
}
