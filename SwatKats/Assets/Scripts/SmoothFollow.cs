using UnityEngine;
using System.Collections;

public class SmoothFollow : MonoBehaviour {

	public Transform targetGO;
	public Transform flightCenter;
	private Vector3 position;
	
	// Use this for initialization
	void Start () {
		position = Vector3.zero;
		}
	
	// Update is called once per frame
	void LateUpdate () {
		
		position = targetGO.transform.position;
		
		transform.position = position;
		transform.LookAt(flightCenter);

	}
}
