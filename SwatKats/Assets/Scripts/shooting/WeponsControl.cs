using UnityEngine;
using System.Collections;

public class WeponsControl : MonoBehaviour {
	
	public Joystick WeponJoystic; 
	public Transform cannon;
	public float timeToLock;
	public int totalMissiles = 4;
	public bool IsCannonReady;
	
	private RaycastHit hit;
	private Transform target;
	//private Transform currTransform;
	private float gameTime;
	private bool lockBegan;
	private float lastLockedTime;
	private int missileLanched;
	private float initialLockTime;
	private float lockTime;

	void Start () {
	
	}
	
	void Update () {
	
		hit = Variables.hitInfo;
		
		//Missile System
		if(CheckTarget(hit,gameTime) && IsMissileReady())
		{
			if(!lockBegan)
			{
				lockTime = initialLockTime+timeToLock;		
			}
			else
			{
				if(gameTime > lockTime)
				{
					target = hit.transform;
					//SendMessage("TargetLocked",target);
					lastLockedTime = gameTime;
					lockBegan = false;
					missileLanched++;
				}
			}
		}
		
		//Cannon
		if(IsCannonReady)
		{
			
		}
		
	
	}
	
	
	public bool CheckTarget(RaycastHit tempHit, float tempGameTime)
	{
		if(tempHit.transform.CompareTag("Enemy"))
		{
			if(!lockBegan)
			{
				lockBegan = true;
				initialLockTime = tempGameTime;
				return true;
			}
			else
				return true;
		}
		else
		{
			lockBegan = false;
			initialLockTime = -1;
			return false;
		}
	}
	

	
	
	public bool IsMissileReady()
	{
		if(missileLanched < totalMissiles && WeponJoystic.IsFingerDown())
		{
			return true;
		}
		return false;
	}
	/*
	void OnGUI()
	{
		if(WeponJoystic.IsFingerDown())
		{
			cannon.SendMessage("FireCannon",true);
			Debug.Log("firing");
		}
	}*/
}
