using UnityEngine;
using System.Collections;

public class DamageControllerTurrent : MonoBehaviour
{
	public Transform target;
	public int totalHealth;
	public int damagePerHit;
	
	private int currHealth;
	
	void Start()
	{
		target = transform;
		currHealth = totalHealth;
	}
	
	void Update ()
	{
		target.SendMessage("SetCurrentHealth",currHealth,SendMessageOptions.DontRequireReceiver);
		if(currHealth <=0 ){
			target.SendMessage("DestroyGO");
		}

	}
	
	void OnCollisionEnter(Collision collision) {
		if(collision.transform.tag == "Bullet"){
			currHealth -= damagePerHit; 
		}
	}
}

