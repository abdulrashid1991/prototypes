using UnityEngine;
using System.Collections;

public class Variables : MonoBehaviour {
	
	public static float flightCurrentVelocity;
	public static RaycastHit hitInfo;
	public static Vector3 cannonFireAngle;
	public static Transform lockedTransform;
	public static bool isFliying;
    public static bool isGameOver;
		
	public void SetCannonFireAngle()
	{
		cannonFireAngle = Targeting.angle;	
	}
	
	public void SetHitInfo()
	{
		hitInfo = Targeting.hit;
		//Debug.Log(hitInfo.point);
	}
	
	public void UpdateVelocity(float velocity)
	{
		flightCurrentVelocity = velocity;
	}
	
	public void IsFying(bool val)
	{
		isFliying = val;
	}
	
	
}
