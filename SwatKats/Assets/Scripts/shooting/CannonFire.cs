using UnityEngine;
using System.Collections;

public class CannonFire : MonoBehaviour {
	
	public Transform bullet;
	public Transform muzzlePT;
	public float timeDelay = 0.01f;
	public AudioSource audioManager;
	public Joystick weponsJoystic;
	
	private bool isReady;
	private float gameTime ;
	private float tempTime = 0.0f;
	private bool firing;
	private bool playing;
	
	void Start () {
		audioManager = (AudioSource)GetComponent(typeof(AudioSource));
	}
	
	void Update () {
	
		gameTime = Time.time;
		
		if(firing)
		{
			if(!playing)
			{
				audioManager.Play();
				audioManager.loop = true;
				playing = true;
			}
		}
		else
		{
			audioManager.Stop();
			playing = false;
		}
	}
	
	
	public bool FireCannon()
	{

		if(IsCannonReady())
		{
			Transform bulletTemp = (Transform)Instantiate(bullet);
			bulletTemp.position = muzzlePT.position;
			bulletTemp.LookAt(Variables.hitInfo.point);
			firing = true;
		}
		return true;
	}
	
	public bool IsCannonReady()
	{
		if(Variables.isFliying && CalcTime())
		{
			return true;
		}
		return false;
	}
	
	public bool CalcTime()
	{	
		if(gameTime > tempTime)
		{
			tempTime = gameTime + timeDelay;
			return true;
		}
		return false;
	}
	
	void OnGUI()
	{
		if(weponsJoystic.IsFingerDown())
		{
			FireCannon();
		}
		else
			firing = false;
	}
}
