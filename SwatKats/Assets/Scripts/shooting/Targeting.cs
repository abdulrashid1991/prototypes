using UnityEngine;
using System.Collections;

public class Targeting : MonoBehaviour {
	
	public Joystick weponsControl;
	public Transform cannon;
	public Camera camera;
	public Transform cube;
	public Transform cannonPivot;
	public float sensitivity = 1;
	public Vector2 offset;
	public int size;
	public Texture crossHair;
	public Rect targetingBoundry;

	public int layer;
	private LayerMask layerMask;
	
	private GUIStyle noGUISTyle = new GUIStyle();
	private bool isOutOfRange_x,isOutOfRange_y;
	private float x,y,y1;
	private Ray ray;
	private Vector2 cPos;
	private float dist = Mathf.Infinity;
	private Vector3 tempPosx,tempPosy;
	
	public static Vector3 angle;
	public static RaycastHit hit;
	public static bool fire;
	
	
	void Start () {
		x = Screen.width / 2;
		y = Screen.height / 4;
		y1 = y*3;
		ray = new Ray();
		hit = new RaycastHit();
		targetingBoundry.width = Screen.width/2;
		targetingBoundry.height = Screen.height/3;
		
		targetingBoundry.x = x - targetingBoundry.width/2;
		targetingBoundry.y = y - targetingBoundry.height/2;
		layer = LayerMask.NameToLayer("Player");
		layerMask = ~layer;
	}
	
	void Update () {
		
		cPos.x = weponsControl.position.x * sensitivity ;
		cPos.y = weponsControl.position.y * sensitivity ;
		
		x += cPos.x;
		y -= cPos.y;
		y1 += cPos.y;
		
		if(targetingBoundry.Contains(new Vector2(x,Screen.height / 4)))
		{
			tempPosx = new Vector3(x,0,0);
		}
		else
		{
			x = tempPosx.x;
		}
		
		if(targetingBoundry.Contains(new Vector2(Screen.width/2,y)))
		{
			tempPosy = new Vector3(0,y,y1);
		}
		else
		{
			y = tempPosy.y;
			y1 = tempPosy.z;
		}
		
		ray = camera.ScreenPointToRay(new Vector3(x,y1,0));
		
		if(Physics.Raycast(ray,out hit))
		{
			//Debug.Log(hit.point);
			//cube.position = hit.point;
			//cube.rotation = Quaternion.LookRotation(hit.normal);
			SendMessage("SetCannonFireAngle");
			SendMessage("SetHitInfo");
			fire = true;
		}
		else
			fire = false;
		
		getAngle(hit,cannonPivot);
	}	
	
	void OnGUI()
	{
		GUI.Box(new Rect(x+offset.x,y+offset.y,size,size),crossHair,noGUISTyle);
	}
	
	public Vector3 getAngle(RaycastHit hit, Transform firingWeponPivot)
	{
		Ray tempRay;
		RaycastHit tempHit;
		if(fire)
		{
			angle = (hit.point - firingWeponPivot.position).normalized;
		}
		else
			angle = transform.forward;
		return angle;
	}
}
