using UnityEngine;
using System.Collections;

public class CannonBullet : MonoBehaviour {
	public float speed;
	private Vector3 angle;
	public Transform explosion;

	
	void Start () {
		Transform flight = (Transform)GameObject.FindWithTag("Player").GetComponent("Transform");
		speed += Variables.flightCurrentVelocity; 
		//explosion = (Transform)Resources.LoadAssetAtPath("Assets/Prefabs/BulletExplosion.prefab",typeof(Transform));
		angle = Variables.cannonFireAngle;
		Physics.IgnoreCollision(transform.collider,flight.collider);
		
	}
	
	void Update () {
		transform.Translate(Vector3.forward * Time.deltaTime * speed );
	}
	
	void OnCollisionEnter(Collision collision)
	{
		Instantiate(explosion,transform.position,transform.rotation);
		Destroy(gameObject);
	}
}
