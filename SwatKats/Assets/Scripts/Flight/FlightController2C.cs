using UnityEngine;
using System.Collections;
public enum FlightState{Idle,TakeOff,Fliying,Boost,Explode};

public class FlightController2C : MonoBehaviour {

	public Joystick flightControl;
	
	public float tiltSpeed;
	public float maxRotationSpeed;
	public float rotationSpeed;

	public Transform flightTransform;
	public Transform explosion;
	public GameObject flight;
	public Transform cannon;
	public AudioSource audioScorce;
    public Camera flightCamera;
    public Transform LFireParticles, RFireParticles;

	private float flightCurrVelocity;
	private float accelaration;
	private bool isFlying = false;
	private bool rotateOk;
	private Rigidbody flightRigidbody;
	private float gameTime,explodeTime;
	private float rotationMultiplier;
	private FlightState currFlightState;
	private bool start = false;
	private bool reload = false;
	private bool init = true;
	private float reloadTime;
    void Awake()
    {
        Variables.isGameOver = false;
    }
	
	void Start () {
		
		flightRigidbody = (Rigidbody)flightTransform.GetComponent(typeof(Rigidbody));
		audioScorce = (AudioSource)GetComponent(typeof(AudioSource));
		explodeTime = Time.time + 40;
		flightRigidbody.useGravity = true;
		cannon.SendMessage("IsFying",isFlying);		
	}
	
	void FixedUpdate () {
        if (Variables.isGameOver)
        {
            StartCoroutine(GameOver());
            return;
        }
		gameTime = Time.time;
		FlightMovment(flightCurrVelocity);
		FlightRotation1();
		if(flightControl.IsFingerDown() && (flightControl.position.x != 0 || flightControl.position.y != 0 ))
		{
			if(rotationMultiplier <= maxRotationSpeed)
			{
				rotationMultiplier +=rotationSpeed;
			}
		}
		else
			rotationMultiplier = 0;

        if (reload)
        {
            if (init)
            {
                reloadTime = gameTime + 5;
                init = false;
            }
            if (reloadTime > gameTime)
            {
                Application.LoadLevel("DemoBak");
                reload = false;
                init = true;
            }
        }
	}

    void LateUpdate()
    {
        if (Variables.isGameOver)
            return;
        if (flightCurrVelocity > 0 && flightCurrVelocity < 2.3f)
        {
            flightCamera.fieldOfView = 60 + flightCurrVelocity * 10;
            LFireParticles.GetComponent<ParticleEmitter>().maxEnergy = flightCurrVelocity/5;
            RFireParticles.GetComponent<ParticleEmitter>().maxEnergy = flightCurrVelocity/5;
        } 
        else if (flightCurrVelocity == 0)
        {
            flightCamera.fieldOfView = 60;
            LFireParticles.GetComponent<ParticleEmitter>().maxEnergy = flightCurrVelocity;
            RFireParticles.GetComponent<ParticleEmitter>().maxEnergy = flightCurrVelocity;
        }
    }

	public void FlightRotation1()
	{
		float pitchLR;
		float rollUD;
		pitchLR = Input.GetAxis("Horizontal");
		rollUD = Input.GetAxis("Vertical");
		
		if(flightControl.IsFingerDown())
		{
			pitchLR = flightControl.position.x;
			rollUD= flightControl.position.y;
		}
		else
		{
			pitchLR = rollUD = 0;
		}
		pitchLR *= rotationMultiplier * Time.deltaTime ;
		rollUD *= rotationMultiplier * Time.deltaTime ;

		//Vector3 rotation = new Vector3(rollUD,pitchLR,0);
		transform.Rotate(rollUD,0,0,Space.Self);
		transform.Rotate(0,pitchLR,0,Space.World);
		if(flightControl.IsFingerDown())
		{
			transform.Rotate(new Vector3(0,0,pitchLR),Space.Self);
		}
		else
		{
			if(transform.localEulerAngles.z != 0)
			{
				if(transform.localEulerAngles.z < 70 && transform.localEulerAngles.z >5)
					transform.Rotate(0,0,-maxRotationSpeed * Time.deltaTime,Space.Self);
				else if(transform.localEulerAngles.z > 290 && transform.localEulerAngles.z < 355 )
					transform.Rotate(0,0,maxRotationSpeed * Time.deltaTime,Space.Self);
			}
		}

	}
	public void FlightRotation2()
	{
		float pitchLR = flightControl.position.x;
		//pitchLR = Input.GetAxis("Horizontal");
		float rollUD = flightControl.position.y;
		//rollUD = Input.GetAxis("Vertical");
		float yawTilt = Input.acceleration.x;
		//float yawTilt = 0; //Input.GetAxis("Tilt");
	
		pitchLR *= rotationMultiplier/2 * Time.deltaTime ;
		rollUD *= rotationMultiplier * Time.deltaTime ;
		yawTilt *= tiltSpeed * Time.deltaTime;
		if(yawTilt != 0)
		{
			if(transform.localEulerAngles.z > 10 && transform.localEulerAngles.z < 350 )
			{
				rollUD += rotationMultiplier/4 * Time.deltaTime;
			}
		}
		
		Vector3 rotation = new Vector3(rollUD,pitchLR,0); 
		transform.Rotate(rotation);
		transform.Rotate(Vector3.forward * yawTilt,Space.Self);
	}
	public void FlightMovment(float velocity)
	{
		transform.Translate(0,0,-velocity);
	}
	public void Throttle(float trottleMultiplier)
	{
		bool trottle;
		accelaration = trottleMultiplier/1000;	
		if(isFlying && flightCurrVelocity > 0 && !start)
		{
			audioScorce.Play();
			start = true;
		}
		audioScorce.pan = flightCurrVelocity/100;
		if(flightCurrVelocity > trottleMultiplier/4)
		{
			trottle = false;
		}
		else 
		{
			trottle = true;
		}
		
		if(trottle)
		{
			flightCurrVelocity += accelaration;				
		}
		if(accelaration < 0.005 && flightCurrVelocity > 0)
		{
			flightCurrVelocity -= 0.005f;
		}
		else if(flightCurrVelocity < 0)
			flightCurrVelocity = 0;
		
		if(accelaration == 0)
		{
			flightRigidbody.useGravity = true;
		}
		else if(accelaration > 0)
		{
			flightRigidbody.useGravity = false;
		}
		
		if(flightControl.tapCount == 2 && flightControl.IsFingerDown() || Input.GetKey(KeyCode.Space))
		{
			BoostFlight(flightCurrVelocity,true);
		}
		else
		{
			BoostFlight(flightCurrVelocity,false);
		}
		cannon.SendMessage("UpdateVelocity",flightCurrVelocity);
	}
	void OnCollisionEnter(Collision collision)
	{
		DontDestroyOnLoad(this);
		if(isFlying && Explode())
		{
			Instantiate (explosion, transform.position, transform.rotation);
            Variables.isGameOver = true;
			Destroy(flight);
		}		
	}
	void OnCollisionExit(Collision collision)
	{
		if(flightCurrVelocity > 0.1f)
		{
			isFlying = true;
			cannon.SendMessage("IsFying",isFlying);
		}
	}
	public bool Explode()
	{
		if(gameTime > explodeTime)
			return true;
		else 
			return false;
	}
	public void MaxRotation(bool val)
	{
		rotateOk = val;
	}
	void DestroyGO()
	{
		Instantiate(explosion,transform.position,transform.rotation);
		Destroy(gameObject,0.5f);
		Debug.Log("Destroy");
		reload = true;
	}

    IEnumerator GameOver()
    {
        Debug.Log("GameOver");
        yield return new WaitForSeconds(2);
        Debug.Log("GameOver");
        Application.LoadLevel("GameOver");
    }
	public void BoostFlight(float currVel,bool canBoost)
	{
		if(canBoost)
		{
            if (flightCurrVelocity < 5 && isFlying)
            {
                flightCurrVelocity += 0.5f;
                flightCamera.fieldOfView += 1;
            }
		}
		else
		{
			flightCurrVelocity = currVel;
			if(flightCurrVelocity > 2.5)
				flightCurrVelocity -= 0.2f;
			if(isFlying)
			{
				//transform.SendMessage("PlaySound",FlightState.Fliying);
			}
		}
	}
}
