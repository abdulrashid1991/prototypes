using UnityEngine;
using System.Collections;

public class GameStateManager : MonoBehaviour {
	public Transform explosion;
	
	void Start()
	{
		if(!explosion)
			explosion = (Transform)Resources.LoadAssetAtPath("Assets/Prefab/LargeExplosion.prefab",typeof(Transform));
	}
	
	void DestroyGO()
	{
		Instantiate(explosion,transform.position,transform.rotation);
		Destroy(gameObject,0.5f);
		Debug.Log("Destroy");
	}
}
