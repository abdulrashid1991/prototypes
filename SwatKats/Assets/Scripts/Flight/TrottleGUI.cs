using UnityEngine;
using System.Collections;

public class TrottleGUI : MonoBehaviour {
	public float trottleMultiplier;
	public Rect rectangle;
	public Transform flight;
    public GUISkin skin;

	void OnGUI(){
        GUI.skin = skin;
		trottleMultiplier = GUI.VerticalSlider(rectangle,trottleMultiplier,10.0f,0.0f);
		flight.SendMessage("Throttle",trottleMultiplier);
	}
	
	
}
