using UnityEngine;
using System.Collections;

public class FollowFlight : MonoBehaviour {

    public Transform targetGO;
    public Joystick flightJoysitc;

	void FixedUpdate() {
        if (flightJoysitc.IsFingerDown())
        {
            Vector3 position = transform.position;
            position.y += flightJoysitc.position.y / 10;
            transform.position = position;
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, targetGO.position, 1 * Time.deltaTime);
        }

	}
}
